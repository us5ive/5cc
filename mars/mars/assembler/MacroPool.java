package mars.assembler;

import java.util.ArrayList;
import java.util.Stack;

import javax.crypto.Mac;

import mars.ErrorList;
import mars.MIPSprogram;

/**
 * Stores information of macros defined by now. <br>
 * Will be used in first pass of assembling MIPS source code. When reached
 * <code>.macro</code> directive, parser calls
 * {@link MacroPool#BeginMacro(String, int)} and skips source code lines until
 * reaches <code>.end_macro</code> directive. then calls
 * {@link MacroPool#CommitMacro(int)} and the macro information stored in a
 * {@link Macro} instance will be added to {@link #macroList}. <br>
 * Each {@link MIPSprogram} will have one {@link MacroPool}<br>
 * NOTE: Forward referencing macros (macro expansion before its definition in
 * source code) and Nested macro definition (defining a macro inside other macro
 * definition) are not supported.
 * 
 * @author M.H.Sekhavat <sekhavat17@gmail.com>
 */
public class MacroPool {
	private MIPSprogram program;
	/**
	 * List of macros defined by now
	 */
	private ArrayList<Macro> macroList;
	/**
	 * @see #BeginMacro(String, int)
	 */
	private Macro current;
	private ArrayList<Integer> callStack;
	/**
	 * @see #getNextCounter()
	 */
	private int counter;

	/**
	 * {@link #counter} will be set to 0 on construction of this class and will
	 * be incremented by each call. parser calls this method once for every
	 * expansions. it will be a unique id for each expansion of macro in a file
	 * 
	 * @return counter value
	 */
	public int getNextCounter() {
		return counter++;
	}

	
	public MacroPool(MIPSprogram mipSprogram) {
		this.program = mipSprogram;
		macroList = new ArrayList<Macro>();
		callStack=new ArrayList<Integer>();
		current = null;
		counter = 0;
	}

	/**
	 * This method will be called by parser when reached <code>.macro</code>
	 * directive.<br>
	 * Instantiates a new {@link Macro} object and stores it in {@link #current}
	 * . {@link #current} will be added to {@link #macroList} by
	 * {@link #CommitMacro(int)}
	 * 
	 * @param name
	 *            name of macro after <code>.macro</code> directive
	 * @param startLine
	 *            line number of <code>.macro</code> directive in source code
	 */
	public void BeginMacro(String name, int startLine) {
		current = new Macro();
		current.setName(name);
		current.setFromLine(startLine);
		current.setProgram(program);
	}

	public Macro getCurrent() {
		return current;
	}

	public void setCurrent(Macro current) {
		this.current = current;
	}

	/**
	 * This method will be called by parser when reached <code>.end_macro</code>
	 * directive. <br>
	 * Adds/Replaces {@link #current} macro into the {@link #macroList}.
	 * 
	 * @param endLine
	 *            line number of <code>.end_macro</code> directive in source
	 *            code
	 */
	public void CommitMacro(int endLine) {
		current.setToLine(endLine);
		current.readyForCommit();
		// (don't!) remove previously defined macro with same name and argument
		// cound
		// macroList.remove(current);
		macroList.add(current);
		current = null;
	}

	/**
	 * Will be called by parser when reaches a macro expansion call
	 * 
	 * @param tokens
	 *            tokens passed to macro expansion call
	 * @return {@link Macro} object matching the name and argument count of
	 *         tokens passed
	 */
	public Macro getMatchingMacro(TokenList tokens, int callerLine) {
		if (tokens.size() < 1)
			return null;
		Macro ret = null;
		Token firstToken = tokens.get(0);
		for (Macro macro : macroList) {
			if (macro.getName().equals(firstToken.getValue())
					&& macro.getArgs().size() + 1 == tokens.size()
					&& macro.getToLine() < callerLine
					&& (ret == null || ret.getFromLine() < macro.getFromLine()))
				ret = macro;
		}
		return ret;
	}

	/**
	 * @param value
	 * @return true if any macros have been defined with name <code>value</code>
	 *         by now, not concerning arguments count.
	 */
	public boolean matchesAnyMacroName(String value) {
		for (Macro macro : macroList)
			if (macro.getName().equals(value))
				return true;
		return false;
	}


	public ArrayList<Integer> getCallStack() {
		return callStack;
	}



	public boolean pushOnCallStack(Integer sourceLine) { //returns true if detected expansion loop
		if (callStack.contains(sourceLine))
			return true;
		callStack.add(sourceLine);
		return false;
	}


	public void popFromCallStack() {
		callStack.remove(callStack.size()-1);
	}


	public String getExpansionHistory() {
		String ret="";
		for (int i=0; i<callStack.size(); i++){
			if (i>0)
				ret+="->";
			ret+=callStack.get(i).toString();
		}
		return ret;
	}
}
