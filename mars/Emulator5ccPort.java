import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Formattable;
import java.util.HashMap;

import mars.ErrorList;
import mars.Globals;
import mars.MIPSprogram;
import mars.ProcessingException;

/**
 * Copyright (C) Us5ive!
 * 
 * This code is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 3, or (at your option) any later version.
 * 
 * This code is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this code; see the file COPYING. If not, write to the Free Software
 * Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 * EndCopyright:
 */

/**
 * Emulates a MIPS32 program and outputs an statistical report of the execution
 * flow.
 */
public class Emulator5ccPort {

    public static void main(String[] args) {
	Globals.initialize(false);
	try {
	    final boolean extendedInstructionsAllowed = true;
	    final boolean warningsAreErrors = false;

	    ArrayList MIPSprogramsToAssemble;
	    Globals.program = new MIPSprogram();
	    ArrayList filesToAssemble = new ArrayList(Arrays.asList(args[0]));
	    String exceptionHandler = null;
	    MIPSprogramsToAssemble = Globals.program.prepareFilesForAssembly(
		    filesToAssemble, (String) filesToAssemble.get(0),
		    exceptionHandler);

	    // added logic to receive any warnings and output them.... DPS
	    // 11/28/06
	    ErrorList warnings = Globals.program.assemble(
		    MIPSprogramsToAssemble, extendedInstructionsAllowed,
		    warningsAreErrors);
	    if (warnings.warningsOccurred()) {
		System.err.println(warnings.generateWarningReport());
	    }

	    Globals.instructionCounter = new int[Globals.program
		    .getMachineList().size()];
	    if (Globals.program.simulate(new int[0])) {
		try (PrintStream ps = new PrintStream(args[1])) {
		    for (int i = 0; i < Globals.program.getMachineList().size(); ++i) {
			ps.println(Globals.instructionCounter[i]);
		    }
		} catch (FileNotFoundException exc) {
		    exc.printStackTrace();
		}
	    }
	} catch (ProcessingException pe) {
	    System.err.println(pe.errors().generateErrorAndWarningReport());
	}
    }

}
