/**
 * Copyright (C) Us5ive!
 * 
 * This code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 * 
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this code; see the file COPYING. If not, write to
 * the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 * EndCopyright:
 */

import java.util.ArrayList;
import java.util.Arrays;

import mars.ErrorList;
import mars.Globals;
import mars.MIPSprogram;
import mars.ProcessingException;
import mars.ProgramStatement;
import mars.assembler.Symbol;
import mars.mips.hardware.AddressErrorException;
import mars.mips.hardware.Memory;
import mars.venus.NumberDisplayBaseChooser;

/**
 * This is a port to MARS's assembler, used by 5cc optimizing compiler.
 */
public class Assembler5ccPort {

    public static void main(String[] args) {
	Globals.initialize(false);
	try {
	    final boolean extendedInstructionsAllowed = true;
	    final boolean warningsAreErrors = false;

	    ArrayList MIPSprogramsToAssemble;
	    Globals.program = new MIPSprogram();
	    ArrayList filesToAssemble = new ArrayList(Arrays.asList(args));
	    String exceptionHandler = null;
	    MIPSprogramsToAssemble = Globals.program.prepareFilesForAssembly(
		    filesToAssemble, (String) filesToAssemble.get(0),
		    exceptionHandler);

	    // added logic to receive any warnings and output them.... DPS
	    // 11/28/06
	    ErrorList warnings = Globals.program.assemble(
		    MIPSprogramsToAssemble, extendedInstructionsAllowed,
		    warningsAreErrors);
	    if (warnings.warningsOccurred()) {
		System.err.println(warnings.generateWarningReport());
	    }

	    System.out.printf("%d\n%d\n", Memory.dataBaseAddress,
		    Memory.textBaseAddress);

	    System.out.println(Globals.program.getLocalSymbolTable().getSize()
		    + Globals.symbolTable.getSize());

	    for (int i = 0; i < Globals.symbolTable.getSize(); ++i) {
		System.out.println(((Symbol) Globals.symbolTable
			.getAllSymbols().get(i)).getName());
		System.out.println(((Symbol) Globals.symbolTable
			.getAllSymbols().get(i)).getAddress());
	    }
	    for (int i = 0; i < Globals.program.getLocalSymbolTable().getSize(); ++i) {
		System.out.println(((Symbol) Globals.program
			.getLocalSymbolTable().getAllSymbols().get(i))
			.getName());
		System.out.println(((Symbol) Globals.program
			.getLocalSymbolTable().getAllSymbols().get(i))
			.getAddress());
	    }

	    ArrayList sourceStatementList = Globals.program.getMachineList();
	    System.out.println(sourceStatementList.size());
	    for (int i = 0; i < Globals.program.getMachineList().size(); ++i) {
		ProgramStatement statement = (ProgramStatement) sourceStatementList
			.get(i);
		System.out.println(statement
			.getPrintableBasicAssemblyStatement());
		System.out.println(statement.getSource());
		System.out.println(statement.getSourceLine());
		System.out.println(NumberDisplayBaseChooser
			.formatUnsignedInteger(statement.getAddress(), 10));
	    }

	    System.out.println(Globals.maxDataOffset + 1);
	    try {
		for (int i = 0; i < Globals.maxDataOffset + 1; ++i) {
		    System.out.printf(
			    "%d\n",
			    Globals.memory.getWord(i * 4
				    + Memory.dataBaseAddress));
		}
	    } catch (AddressErrorException exc) {
		System.err.println("You shouldn't see this. --Sahand");
		exc.printStackTrace();
	    }

	} catch (ProcessingException pe) {
	    System.err.println(pe.errors().generateErrorAndWarningReport());
	}
    }
}
