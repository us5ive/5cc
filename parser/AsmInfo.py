'''
Created on Jan 11, 2013

@copyright:
Copyright (C) Us5ive! 
This code is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3, or (at your option)
any later version.

This code is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this code; see the file COPYING.  If not, write to
the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
'''

from parser.Instruction import Instruction
from parser.InstructionList import InstructionList
from parser.MemoryMap import MemoryMap

class AsmInfo(object):
	'''Holds the data extracted out of an assembly file.'''

	symbol_table = {}
	instructions = None  # List of instructions.
	memory = None  # Simulates memory
	file_name = None
	label2jumper = {}  # Maps each label to the set of instructions pointing to it.

	def __init__(self, file, text_base_address, data_base_address):
		self.file_name = file
		self.instructions = InstructionList(text_base_address)
		self.memory = MemoryMap(data_base_address)

	def get_new_label(self, label):
		'''Generates a new label by appending a unique number to the end of the
		given label, so that the new label does not coincide with another label.
		@param label: New label's preferred name.
		@return: A new label which is not already in the symbol table.
		'''

		if not label in self.symbol_table :
			return label
		else :
			i = 0
			new_label = label + "_" + str(i)
			while new_label in self.symbol_table :
				i += 1
				new_label = label + "_" + str(i)
			return new_label

	def copy(self):
		'''Deep copy'''

		clone = AsmInfo(self.file_name, self.instructions.offset, self.memory.offset)
		clone.symbol_table = self.symbol_table.copy()
		clone.instructions = self.instructions.copy()
		clone.memory = self.memory.copy()
		clone.label2jumper = {}
		for lab in self.label2jumper :
			clone.label2jumper[lab] = set()
			for j in self.label2jumper[lab] :
				clone.label2jumper[lab].add(clone.instructions[j.address])
		return clone

	def remove_instructions(self, from_, to_):
		'''Removes a sequence of instructions from the text. Then iterates
		over the proceeding instructions and revalidates their addresses. Also
		moves the labels accordingly.
		If an instruction is being removed and it has a label pointing to it,
		the label will be passed to the next instruction if it doesn't have one,
		otherwise the label is removed from the code and from the symbol table. 
		(for sake of omitting redundant labels.)
		Note: Don't remove the dummy instruction at the end of text. It is
		responsible for binding the label after the final instruction (if any)
		deleting it might break this function.
		@param from_: address or index of beginning of the sequence to be
				removed (inclusive)
		@param to_: address or index of the end of the sequence to be removed
				(exclusive).
		'''

		# Normalize addressing.
		if from_ >= self.instructions.offset:
			from_ = (from_ - self.instructions.offset) >> 2
			to_ = (to_ - self.instructions.offset) >> 2
		if to_ < 0:
			to_ = from_ + 1
		if to_ > self.instructions.__len__():
			to_ = self.instructions.__len__()
		to_inst = self.instructions[to_]

		# Remove instructions.
		trash = self.instructions.remove_instructions(from_, to_)

		# Identify affected labels.
		homeless_label = None
		homeless_jumpers = set()
		node = trash
		for i in range(from_, to_):
			if node.label:
				homeless_label = node.label
				self.symbol_table.pop(homeless_label)
				self.instructions.labels.pop(homeless_label)
				for x in self.label2jumper.pop(homeless_label):
					if (x.address - self.instructions.offset) >> 2 < from_ or \
					(x.address - self.instructions.offset) >> 2 >= to_ :
						homeless_jumpers.add(x)
			target = node.get_target_address()
			if target and target in self.label2jumper.keys():
				self.label2jumper[target].remove(node)
			node = node.next

		# Move the label of removed instructions (if any) to the instruction,
		# immediately following the removed block.
		if homeless_label :
			if to_inst.label is None:
				to_inst.label = homeless_label
				self.label2jumper[homeless_label] = set()
				self.symbol_table[homeless_label] = to_
				self.instructions.labels[homeless_label] = to_inst
			for jumper in homeless_jumpers :
				self.label2jumper[to_inst.label].add(jumper)

		# Redirect instructions that previously pointed to the removed block, to
		# the instruction immediately following the block.
		new = to_inst.label
		for j in homeless_jumpers:
			old = j.get_target_address()
			for i in range(0, 3):
				if j.args[i] == old:
					j.args[i] = new

		# Revalidate instruction addresses.
		from_ = from_ * 4 + self.instructions.offset
		to_ = to_ * 4 + self.instructions.offset
		address = from_
		node = to_inst
		while node :
			node.address = address
			if(node.label):
				self.symbol_table[node.label] = address
			target = node.get_target_address()
			if target and target in self.label2jumper.keys():
				self.label2jumper[target].remove(node)
				self.label2jumper[target].add(node)
			address += 4
			node = node.next

	def remove_instruction(self, index):
		'''Removes a single instruction out of the code. If the instruction has
		got a label pointing to it, the label will be directed to the next
		instruction if it doesn't have a label. Otherwise the label is removed 
		from the code and from the symbol table.
		Note: Don't try to remove the dummy instruction in the final line. It
		is responsible for binding the label pointed after the last instruction
		(if any). Removing it might break the code.
		@param index: index or address of the instruction to be removed.
		'''

		if index < self.instructions.offset :
			self.remove_instructions(index, index + 1)
		else:
			self.remove_instructions(index, index + 4)

	def move_instructions(self, src_start, src_end, dest):
		'''Moves a sequence of instructions from one location to another.
		Of course nothing changes if the destination falls within the sequence.
		@param src_start: Beginning of the sequence to be moved. (inclusive)
		@param src_end: End of the sequence to be moved. (exclusive)
		@param dest: Destination slot. Slots are numbered from zero, starting 
		from behind first instruction.
		'''

		# Normalize instruction addressing
		if src_start >= self.instructions.offset:
			src_start = (src_start - self.instructions.offset) >> 2
			src_end = (src_end - self.instructions.offset) >> 2

		# Stop if the destination falls within the block itself.
		if(dest <= src_end and dest >= src_start):
			return

		address = min(src_start, dest) * 4 + self.instructions.offset
		node = self.instructions[address]
		if node.prev :
			node = node.prev
		else :
			node = None
		# Move instructions.
		self.instructions.move_instructions(src_start, src_end, dest)

		if node:
			node = node.next
		else:
			node = self.instructions.root
		# Revalidate instruction addresses.
		while address < max(src_end, dest) * 4 + self.instructions.offset:
			node.address = address
			if node.label :
				self.symbol_table[node.label] = address
			target = node.get_target_address()
			if target and target in self.label2jumper.keys():
				self.label2jumper[target].remove(node)
				self.label2jumper[target].add(node)
			address += 4
			node = node.next

	def copy_instructions(self, src_start, src_end, dest):
		'''Copies a sequence of instructions to a new destination.
		@param src_start: Beginning of the sequence to be moved. (inclusive)
		@param src_end: End of the sequence to be moved. (exclusive)
		@param dest: Destination slot. Slots are numbered from zero, starting 
		from behind first instruction.
		'''

		# Normalize instruction addressing
		if src_start >= self.instructions.offset:
			src_start = (src_start - self.instructions.offset) >> 2
			src_end = (src_end - self.instructions.offset) >> 2

		copy = []
		node = self.instructions[src_start]
		for i in range(src_start, src_end) :
			copy.append(node.copy())
			node = node.next
		for i in range(0, src_end - src_start - 1) :
			copy[i].next = copy[i + 1]
			copy[i + 1].prev = copy[i]
		label2copy = {}
		for i in range(0, src_end - src_start) :
			if copy[i].label :
				new_label = self.get_new_label(copy[i].label)
				label2copy[copy[i].label] = new_label
				copy[i].label = new_label
		for i in range(0, src_end - src_start) :
			target = copy[i].get_target_address()
			if target :
				for j in (0, 3) :
					if copy[i].args[j] == target :
						copy[i].args[j] = label2copy[target]
		self.insert_instructions(copy, dest)

	def insert_instructions(self, inst_list, dest):
		'''Inserts a list of instructions into the specified location. It is
		assumed that the inserted instructions do not contain redundant labels
		or dangling jump targets.
		@param inst_list: List of instructions to be inserted.
		@param dest: Destination slot. Slots are numbered from zero, starting 
		from behind first instruction.
		'''

		# Degeneracies
		if inst_list.__len__() == 0 :
			return

		# Normalize
		if dest >= self.instructions.offset :
			dest = (dest - self.instructions.offset) >> 2

		# Insert the instructions.
		self.instructions.insert_instructions(inst_list, dest)

		# Revalidate the instruction addresses.
		node = inst_list[0]
		for address in range(dest * 4 + self.instructions.offset,
							self.instructions.__len__() * 4 +
							self.instructions.offset, 4) :
			node.address = address
			if node.label :
				self.symbol_table[node.label] = address
				if not node.label in self.label2jumper :
					self.label2jumper[node.label] = set()
				if not node.label in self.instructions.labels :
					self.instructions.labels[node.label] = node
			target = node.get_target_address()
			if target and not target.startswith("$") :
				if not target in self.label2jumper:
					self.label2jumper[target] = set()
				self.label2jumper[target].add(node)
			node = node.next

	def put_label(self, address, label):
		'''Puts a label on the given instruction. If it already has one, it will
		be replaced. It is assumed that the new label did not previously exist.
		@param address: Address of the instruction to get the new label. This
		can be specified by means of both address or index.
		@param label: The label to be put on the instruction.
		'''

		# Normalize instruction addressing.
		if address >= self.instructions.offset:
			address = (address - self.instructions.offset) >> 2

		# Update necessary informations.
		node = self.instructions[address]
		if node.label :
			self.symbol_table.pop(node.label)
			self.instructions.labels.pop(node.label)
			self.label2jumper[label] = self.label2jumper.pop(node.label)
			for j in self.label2jumper[label] :
				for i in range(0, 3) :
					if j.args[i] == node.label :
						j.args[i] = label
		else:
			self.label2jumper[label] = set()

		# Put labels.
		self.symbol_table[label] = address
		self.label2jumper[label] = set()
		self.instructions.labels[label] = self.instructions[address]
		node.label = label

	def prune_labels(self):
		'''Removes unnecessary labels.'''

		for label in self.symbol_table.copy() :
			if label in self.label2jumper and \
			self.label2jumper[label].__len__() == 0 and label != "main":
				self.instructions.labels.pop(label)
				self.instructions[self.symbol_table.pop(label)].label = None
				self.label2jumper.pop(label)

	def move_label(self, label, address):
		'''Moves the given label to the given address. It is assumed that the
		label exists in the symbol table and the address is a valid memory
		address or instruction index.
		@param label: Label
		@param address: Address
		'''

		# Normalize addressing
		if address < self.instructions.offset :
			address = address * 4 + self.instructions.offset

		old_inst = self.instructions[self.symbol_table[label]]
		new_inst = self.instructions[address]
		old_inst.label = None
		new_inst.label = label
		self.symbol_table[label] = address
		self.instructions.labels[label] = new_inst

	def change_instruction(self, address, name, arg0=None, arg1=None, arg2=None):
		'''Changes name and arguments of the given instruction.
 		@param address: Address of the instruction, either in terms of memory
 		address or index.
 		@param name: New name of the instruction.
 		@param arg0: New first argument (defaults to None).
 		@param arg1: New second argument (defaults to None).
 		@param arg2: New third argument (defaults to None).
 		'''

		# Normalize addressing
		if address < self.instructions.offset :
			address = address * 4 + self.instructions.offset

		# Change name
		self.instructions[address].name = name

		# Stop linking to the jump target if any.
		target = self.instructions[address].get_target_address()
		if target and not target.startswith("$") :
			self.label2jumper[target].remove(address)

		# Change arguments.
		self.instructions[address].args = [arg0, arg1, arg2]

		# Link to the new jump target if any.
		target = self.instructions[address].get_target_address()
		if target and not target.startswith("$") :
			self.label2jumper[target].add(address)
