'''
Created on Jan 15, 2013

@copyright: 
Copyright (C) Us5ive!
This code is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3, or (at your option)
any later version.

This code is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this code; see the file COPYING.  If not, write to
the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
'''

class InstructionList():
	'''A linked list for holding and manipulating the instructions.
	'''

	offset = -1		# Starting address of instructions in memory. 
	root = None		# First instruction.
	labels = {}		# Maps each label to its instruction. 
	_tail = None
	_size = 0
		
	def __init__(self, offset):
		self.offset = offset
				
	def __len__(self):
		return self._size
		
	def append(self, inst):
		self._size += 1
		if self.root :
			self._tail.next = inst
			inst.prev = self._tail
			self._tail = self._tail.next
		else:
			self.root = inst
			self._tail = inst
	
	def copy(self):
		clone = InstructionList(self.offset)
		clone.root = self.root.copy()
		self_tail = self.root.next
		clone._tail = clone.root
		
		if(clone.root.label) :
			clone.labels[clone.root.label] = clone.root
		
		while(self_tail) :
			clone._tail.next = self_tail.copy()
			clone._tail.next.prev = clone._tail
			self_tail, clone._tail = self_tail.next, clone._tail.next
			if(clone._tail.label) :
				clone.labels[clone._tail.label] = clone._tail
			
		clone._size = self._size
		
		
		return clone
	
	def remove_instructions(self, from_, to_):
		'''Removes a sequence of instructions from the text.
		Note: use the method with same name in AsmInfo.
		@param from_: address or index of beginning of the sequence to be
				removed (inclusive)
		@param to_: address or index of the end of the sequence to be removed
				(exclusive).
		'''

		# Normalize addressing.
		if from_ >= self.offset:
			from_ = (from_ - self.offset) >> 2
			to_ = (to_ - self.offset) >> 2
		if to_ < 0:
			to_ = from_ + 1
		if to_ > self.__len__():
			to_ = self.__len__()
			
		start = self[from_]
		stop = self[to_]
		
		if(start.prev):
			start.prev.next = stop
		if(stop):
			stop.prev.next = None
			stop.prev = start.prev
		if not (start.prev) :
			self.root = stop
		
		self._size -= (to_ - from_)
		
		start.prev = None
		return start
	
	def move_instructions(self, src_start, src_end, dest):
		'''Moves a sequence of instructions from one location to another.
		Of course nothing changes if the destination falls within the sequence.
		Note: Do not use this method. Instead use the method with the same name,
		from the AsmInfo class.
		@param src_start: Beginning of the sequence to be moved. (inclusive)
		@param src_end: End of the sequence to be moved. (exclusive)
		@param dest: Destination slot. Slots are numbered from zero, starting 
		from behind first instruction.
		'''
	
		# Normalize instruction addressing
		if src_start >= self.offset:
			src_start = (src_start - self.offset) >> 2
			src_end = (src_end - self.offset) >> 2
			
		# Stop if the destination falls within the block itself.
		if(dest <= src_end and dest >= src_start):
			return

		f = self[src_start]
		l = self[src_end].prev
		if dest != 0 and dest != self.__len__():
			d = self[dest]
		
		if f.prev :
			f.prev.next = l.next
		else:
			self.root = l.next
			
		if l.next :
			l.next.prev = f.prev
		else :
			self._tail = f.prev
		
		if dest == 0 :
			l.next = self.root;
			self.root = f
			f.prev = None
		elif dest == self.__len__() :
			l.next = None
			self._tail.next = f
			f.prev = self._tail
			self._tail = l
		else :
			d.prev.next = f
			f.prev = d.prev
			d.prev = l
			l.next = d
			
	def insert_instructions(self, inst_list, dest):
		'''Inserts a list of instructions into the specified location.
		Note: Do not use this method. Instead use the method with the same name,
		from the AsmInfo class.
		@param inst_list: List of instructions to be inserted.
		@param dest: Destination slot. Slots are numbered from zero, starting 
		from behind first instruction.
		'''

		f = inst_list[0]
		t = f
		for i in range(1, inst_list.__len__()) :
			if t.label :
				self.labels[t.label] = t
			inst_list[i].prev = t
			t.next = inst_list[i]
			t = t.next
		t.next = None

		if dest == 0 :
			t.next = self.root
			self.root.prev = t
			self.root = f
		elif dest == self.__len__() :
			self._tail.next = f
			f.prev = self._tail
			self._tail = t
		else :
			d = self[dest]
			d.prev.next = f
			f.prev = d.prev
			d.prev = t
			t.next = d
			
		self._size += inst_list.__len__()
			
	def __iter__(self):
		def list_iterator():
			n = self.root
			while n :
				yield n
				n = n.next
		return list_iterator()
	
	def __getitem__(self, key):
		if isinstance(key, slice) :
			return self.__getitem__(key.start)
		elif(key < self.offset):
			return self.__getitem__(key * 4 + self.offset)
		
		hook = self.labels["main"]
		dist = abs(key - hook.address)
		for label in self.labels :
			if abs(self.labels[label].address - hook.address) < dist:
				dist = abs(self.labels[label].address - hook.address)
				hook = self.labels[label]
		
		while(hook and hook.address > key) :
			hook = hook.prev

		while(hook and hook.address < key) :
			hook = hook.next
		if hook :
			return hook
		else:
			raise StopIteration
	
	def __setitem__(self, key, value):
		if isinstance(key, slice) :
			if key.start < self.offset :
				return self.__setitem__(slice(key.start * 4 + self.offset,
											key.stop * 4 + self.offset, 4), value)
			k = key.start
			for node in value :
				self.__setitem__(k, node)
				k += 4
		else:
			if key < self.offset :
				key = key * 4 + self.offset
			inst = self.__getitem__(key)
			if(inst.prev) :
				inst.prev.next = value
			if(inst.next) :
				inst.next.prev = value
			value.next, value.prev = inst.next, inst.prev
			value.label = inst.label
			if value.label :
				self.labels[value.label] = value
			value.address = inst.address
			if value.address == self.offset :
				self.root = value
