'''
Created on Jan 11, 2013

@copyright:
Copyright (C) Us5ive! 
This code is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 3, or (at your option) any later version.

This code is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this code; see the file COPYING.  If not, write to the Free Software Foundation,
675 Mass Ave, Cambridge, MA 02139, USA.
'''

import sys
import re

BRANCHERS = {"bc1f", "bc1t", "beq", "bgez", "bgezal", "bgtz", "blez", "bltz",
		     "bltzal", "bne"}
BRANCHERS_WO_RETURN = {"bclf", "bclt", "beq", "bgez", "bgtz", "blez", "bltz",
		     "bne"}
EXITERS = {"break", "syscall"}
JUMPERS = {"eret", "j", "jal", "jalr", "jr"}
UNCONDITIONAL_JUMPERS_WO_RETURN = {"break", "eret", "j", "jr"}

_JUMPERS_0_1 = {"bclf", "bclt"}
_JUMPERS_0 = {"j", "jal", "jr"}
_JUMPERS__0_1 = {'jalr'}
_JUMPERS_1 = {"bgez", "bgezal", "bgtz", "blez", "bltz", "bltzal", "jalr"}
_JUMPERS_2 = {"beq", "bne"}
_JUMPERS_END = {"break", "syscall"}
_JUMPERS_TRAP = {"eret", "teq", "teqi", "tge", "tgei", "tgeiu", "tgeu", "tlt",
					"tlti", "tltiu", "tlt"}

_WRITE_arg0 = {'abs.s', 'abs.d', 'add', 'add.d', 'add.s', 'addi', 'addiu', 'addu',
			'and', 'andi', 'ceil.w.d', 'ceil.w.s', 'clo', 'clz', 'cnv.d.s',
			'cnv.d.w', 'cnv.s.d', 'cnv.s.w', 'cnv.w.d', 'cnv.w.s', "div.d",
			'div.s', 'floor.w.d', 'floor.w.s', 'lb', 'lbu', 'ldc1', 'lh', 'lhu',
			'll', 'lui', 'lw', 'lwc1', 'lwl', 'lwr', 'mfc0', 'mfc1', 'mfhi',
			'mflo', 'mov.d', 'mov.s', 'movf', 'mov.d', 'mov.s', 'movn', 'mov.d',
			'mov.s', 'movt', 'movt.d', 'movt.s', 'movz', 'movz.d', 'movz.s',
			'mul', 'mul.d', 'mul.s', 'neg.d', 'neg.s', 'nor', 'or', 'ori',
			'round.w.d', 'round.w.s', 'sll', 'sllv', 'slt', 'slti', 'sltiu',
			'sltu', 'sqrt.d', 'sqrt.s', 'sra', 'srav', 'srl', 'srlv', 'sub',
			'sub.d', 'sub.s', 'subu', 'trunc.w.d', 'trunc.w.s', 'xor', 'xori'}
_WRITE_arg1 = {'sb', 'sdc1', 'sh', 'sw', 'swc1', 'swl', 'swr'}
_WRITE_arg0_arg1 = {'sc'}
_WRITE_C0_ = {'mtc0'}
_WRITE_C1F__arg0 = {"c.eq.d", "c.eq.s", "c.le.d", "c.le.s", "c.lt.d", "c.lt.s", 'mtc1'}
_WRITE_LO_HI_arg0 = {'mul'}
_WRITE_LO_HI = {"div", "divu", 'madd', 'maddu', 'msub', 'msubu', 'mult', 'multu'}
_WRITE_LO = {'mtlo'}
_WRITE_HI = {'mthi'}
_WRITE_RA = {'beqzal', 'bgezal', 'bltzal', 'jal'}
_WRTIE__RA_arg1 = {'jalr'}

_READ_arg0 = {'bgez', 'bgezal', 'bgtz', 'blez', 'bltz', 'bltzal', 'jr', 'mtc1',
			'sb', 'sc', 'sdc1', 'sh', 'sw', 'swc1', 'swl', 'swr'}
_READ_arg0_arg1 = {'beq', 'bne', 'div', 'div.d', 'div.s', 'divu', 'mult', 'multu',
				'teq', 'teqi', 'tge', 'tgei', 'tgeiu', 'tgeu', 'tlt', 'tlti',
				'tltiu', 'tltu', 'tne', 'tnei'}
_READ_arg0_arg1_LO_HI = {'madd', 'maddu', 'msub', 'msubu'}
_READ_arg0_basereg = {'sb', 'sc', 'sdc1', 'sh', 'sw', 'swc1', 'swl', 'swr'}
_READ_arg1 = {'abs.d', 'abs.s', 'ceil.w.d', 'ceil.w.s', 'clo', 'clz', 'cnv.d.s',
			'cnv.d.w', 'cnv.s.d', 'cnv.s.w', 'cnv.w.d', 'cnv.w.s', 'floor.w.d',
			'floorw.s', 'mfc1', 'mov.d', 'mov.s', 'neg.d', 'neg.s', 'round.w.d',
			'round.w.s', 'sqrt.d', 'sqrt.s', 'trunc.w.d', 'trunc.w.s'}
_READ_arg1_basereg = {'lb', 'lbu', 'ldc1', 'lh', 'lhu', 'll', 'lw', 'lwc1',
					'lwl', 'lwr'}
_READ_arg1_arg2 = {'add', 'add.d', 'add.s', 'addi', 'addiu', 'addu', 'and',
				'andi', 'movn', 'movn.d', 'movn.s', 'movz', 'movz.d', 'movz.s',
				'mul', 'mul.d', 'mul.s', 'nor', 'or', 'ori', 'sll', 'sllv',
				'slt', 'slti', 'sltiu', 'sltu', 'sra', 'srav', 'srl', 'srlv',
				'sub', 'sub.d', 'sub.s', 'subu', 'xor', 'xori'}
_READ_C0_ = {'mfc0'}
_READ__arg0__arg1 = {'jalr'}
_READ__arg0_1_arg1_2 = {'c.eq.d', 'c.eq.s', 'c.le.d', 'c.le.s', 'c.lt.d', 'c.lt.s'}
_READ_C1F__arg0 = {'bclf', 'bclt'}
_READ_C1F__arg2_arg1 = {'movf', 'movf.d', 'movf.s', 'movt', 'movt.d', 'movt.s'}
_READ_LO = {'mflo'}
_READ_HI = {'mfhi'}
_count = 0

class Instruction(object):
	'''Represents an instruction of MIPS32 assembly'''

	name = None  # Instruction name
	args = None  # List of arguments
	line_no = -1  # Instruction's line number in the original code.
	address = -1  # Instruction's ddress of in memory.
	source = ""  # Instruction's original source code, from which it was parsed.
	label = None  # The label pointing to this instruction (or None).
	next = None  # Points to the next instruction (in the order they appear in the memory).
	prev = None  # Points to the previous instruction (in the order they appear in the memory).
	id = -1  # Unique ID.

	def get_target_address(self):
		'''If the instruction is of jump or branch type, it returns the argument
		holding the jump address. Otherwise returns None
		@return: Jump address or None.
		'''

		if(self.name in _JUMPERS_0_1):
			if(self.args[0].startswith('$')):
				return self.args[1]
			else:
				return self.args[0]
		elif(self.name in _JUMPERS_0):
			return self.args[0]
		elif self.name in _JUMPERS__0_1 :
			if self.args[1] :
				return self.args[0]
			else:
				return "$ra"
		elif(self.name in _JUMPERS_1):
			return self.args[1]
		elif(self.name in _JUMPERS_2):
			return self.args[2]
		elif(self.name in _JUMPERS_TRAP):
			sys.stderr.write("Trap instructions are not supported yet: " +
							self.name)
			return None
		else:
			return None

	def writes(self):
		'''Determines the memory or memories that change value by execution of
		this instruction.
		@return: The memory on which a write is performed, or None. The output
		can be a register (e.g $t0, $f1), a coprocessor flag (e.g $c1.f0),
		a memory location (e.g 0x0000001C($t0)) or None.
		'''

		if self.name == "syscall" :
			return None  # TODO predict value of $v0
		elif self.name in _WRITE_arg0 :
			return (self.args[0],)
		elif self.name in _WRITE_arg1 :
			return (self.args[1],)
		elif self.name in _WRITE_arg0_arg1 :
			return (self.args[0], self.args[1])
		elif self.name in _WRITE_C0_ :
			return ("$c0." + self.args[1][1:],)
		elif self.name in _WRITE_C1F__arg0 :
			if self.args[0] :
				return ("$c1.f" + self.args[0],)
			else :
				return ("$c1.f0",)
		elif self.name in _WRITE_LO_HI_arg0 :
			return (self.args[0], "$lo", "$hi",)
		elif self.name in _WRITE_LO_HI :
			return ('$lo', '$hi',)
		elif self.name in _WRITE_LO :
			return ('$lo',)
		elif self.name in _WRITE_HI :
			return ('$hi',)
		elif self.name in _WRITE_RA :
			return ("$ra",)
		elif self.name in _WRTIE__RA_arg1 :
			if self.args[1] :
				return (self.args[0],)
			else :
				return ("$ra",)
		else :
			return None

	def reads(self):
		'''Determines the memory or memories that get accessed by execution of
		this instruction.
		@return: The memory on which a read is performed, or None. The output
		can be a register (e.g $t0, $f1), a coprocessor flag (e.g $c1.f0),
		a memory location (e.g 0x0000001C($t0)), an immediate value (e.g 0x0010)
		or None.
		'''

		if self.name == "syscall" :
			return ("$2",)  # TODO predict value of $v0.
		elif self.name in _READ_arg0 :
			return (self.args[0],)
		elif self.name in _READ_arg0_arg1 :
			return (self.args[0], self.args[1],)
		elif self.name in _READ_arg0_arg1_LO_HI :
			return (self.args[0], self.args[1], "$lo", "$hi",)
		elif self.name in _READ_arg0_basereg :
			return (self.args[0], re.sub(re.compile(r'[-+]?\d+\((\$\d+)\)'), r'\1', self.args[1]))
		elif self.name in _READ_arg1 :
			return (self.args[1],)
		elif self.name in _READ_arg1_arg2 :
			return (self.args[1], self.args[2],)
		elif self.name in _READ_arg1_basereg :
			return (self.args[1], re.sub(re.compile(r'[-+]?\d+\((\$\d+)\)'), r'\1', self.args[1]))
		elif self.name in _READ_C0_ :
			return ("$c0." + self.args[1][1:],)
		elif self.name in _READ__arg0__arg1 :
			if self.args[1] :
				return self.args[1]
			else:
				return self.args[0]
		elif self.name in _READ__arg0_1_arg1_2 :
			if self.args[2] :
				return (self.args[1], self.args[2])
			else:
				return (self.args[0], self.args[1])
		elif self.name in _READ_C1F__arg0 :
			if self.args[1] :
				return ("$c1.f" + self.args[0],)
			else :
				return ("$c1.f0",)
		elif self.name in _READ_C1F__arg2_arg1 :
			if self.args[2] :
				return ("$c1f" + self.args[2], self.args[1])
			else :
				return ("$c1f0", self.args[1])
		else :
			return None

	def is_jumper(self):
		return self.name in JUMPERS

	def is_brancher(self):
		return self.name in BRANCHERS

	def is_exiter(self):
		return self.name in EXITERS

	def is_brancher_wo_return(self):
		return self.name in BRANCHERS_WO_RETURN

	def is_unconditional_jumper_wo_return(self) :
		return self.name in UNCONDITIONAL_JUMPERS_WO_RETURN

	def copy(self):
		global _count
		clone = Instruction(self.name, *self.args)
		clone.line_no = self.line_no
		clone.address = self.address
		clone.source = self.source
		clone.label = self.label
		clone.id = _count
		_count += 1
		return clone

	def __init__(self, name, arg0=None, arg1=None, arg2=None):
		global _count
		self.name = name
		self.args = [arg0, arg1, arg2]
		self.id = _count
		_count += 1

		def hex2dec(match):
			temp = int(match.group(0), 16)
			if temp > 0x8FFFFFFF :
				temp = -(0xFFFFFFFF - temp + 1)
			return str(temp)
		def oct2dec(match):
			return str(int(match.group(0), 8))
		def bin2dec(match):
			return str(int(match.group(0), 2))

		for i in range(0, 3):
			if self.args[i]:
				self.args[i] = re.sub("^0[xX][0-9a-fA-F]+", hex2dec, str(self.args[i]))
				self.args[i] = re.sub("^0[oO][0-8]+", oct2dec, str(self.args[i]))
				self.args[i] = re.sub("^0[bB][01]+", bin2dec, str(self.args[i]))

	def __str__(self):
		result = self.name
		if(self.label != None):
			result = self.label + ": " + result
		for arg in self.args:
			if arg is not None:
				result += " " + str(arg)
		return result

	def __repr__(self):
		result = self.name
		if(self.label != None):
			result = self.label + ": " + result
		for arg in self.args:
			if arg is not None:
				result += " " + str(arg)
		return result

	def __hash__(self):
		return self.id

	def __eq__(self, other):
		return other != None and self.id == other.id
