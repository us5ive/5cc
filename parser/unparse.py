'''
Created on Jan 12, 2013

@copyright: 
Copyright (C) Us5ive!
This code is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3, or (at your option)
any later version.

This code is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this code; see the file COPYING.  If not, write to
the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
'''

import re

def unparse(asm, output):
	'''Converts an AsmInfo instance into an emulator-parsable format and saves
	that on disk.'''

	def dec2hex(match):
		input_ = int(match.group(0))
		if input_ < 0 :
			input_ = ~(input_+1)
		return " %#x" % (input_,)

	file = open(output, "w")
	file.write(".text\n")

	for instruction in asm.instructions:
		string = re.sub(r" [-+]?\d+($| )", dec2hex, str(instruction))
		file.write(string + "\n")

	file.write("\n.data\n.align 0\n.byte\n")
	inverse_sym_table = []
	for sym in asm.symbol_table.keys():
		if asm.symbol_table[sym] >= asm.memory.offset:
			inverse_sym_table.append((asm.symbol_table[sym], sym))
	inverse_sym_table.sort()
	address = asm.memory.offset
	for word in asm.memory:
		for mask in [0x000000FF, 0x0000FF00, 0x00FF0000, 0xFF000000]:
			if inverse_sym_table.__len__() > 0 and inverse_sym_table[0][0] == address :
				file.write("\n" + inverse_sym_table[0][1] + ":\n")
				inverse_sym_table[0:1] = []
			file.write("%d " % (word & 0x000000FF))
			word >>= 8
			address += 1
			
if __name__ == '__main__':
	pass
