'''
Created on Jan 11, 2013

@copyright: 
Copyright (C) Us5ive!
This code is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3, or (at your option)
any later version.

This code is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this code; see the file COPYING.  If not, write to
the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
'''

class MemoryMap(list):
	'''A subclass of List, modified to be used as a RAM model. Each element
	represents a memory word. Data can be accessed both by means of memory
	address and by index.'''

	offset = 0

	def __getitem__(self, key):
		if isinstance(key, slice) :
			if(key.start >= self.offset):
				return list.__getitem__(self, slice((key.start - self.offset) >> 2,
												(key.stop - self.offset) >> 2,
												key.step))
		elif(key >= self.offset):
			return list.__getitem__(self, (key - self.offset) >> 2)
		return list.__getitem__(self, key)

	def __setitem__(self, key, value):
		if isinstance(key, slice) :
			if(key.start >= self.offset):
				return list.__setitem__(self, slice((key.start - self.offset) >> 2,
								(key.stop - self.offset) >> 2, key.step), value)
		elif(key >= self.offset):
			return list.__setitem__(self, (key - self.offset) >> 2, value)
		return list.__setitem__(self, key, value)

	def __init__(self, offset):
		self.offset = offset
		
	def copy(self):
		clone = MemoryMap(self.offset)
		for mem in self:
			clone.append(mem)
		return clone

	def lw(self, address):
		'''Load word'''
		return self[address]

	def sw(self, address, value):
		'''Store word'''
		self[address] = value

	def lh(self, address):
		'''Load half word'''
		return list.__getitem__(self, (address - self.offset) >> 1)

	def sh(self, address, value):
		'''Store half word'''
		list.__setitem__(self, (address - self.offset) >> 1, value)

	def lb(self, address):
		'''Load byte'''
		return list.__getitem__(self, address - self.offset)

	def sb(self, address, value):
		'''Store byte'''
		list.__getitem__(self, address - self.offset, value)
