'''
Created on Jan 11, 2013

@copyright: 
Copyright (C) Us5ive!
This code is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3, or (at your option)
any later version.

This code is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this code; see the file COPYING.  If not, write to
the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
'''

from parser.AsmInfo import AsmInfo
from parser.Instruction import Instruction
from parser.MemoryMap import MemoryMap
import re
import subprocess


def _fix_jump_label(inst, sym_table, label2address):
	'''Changes the jump and branch addresses back to labels.'''

	if inst.is_jumper() or inst.is_brancher():
		if inst.is_jumper():
			target = inst.get_target_address()
		elif inst.is_brancher():
			temp = int(inst.get_target_address())
			if temp > 0X8FFFFFFF :
				temp = -(0xFFFFFFFF - temp + 1)
			target = str(inst.address + temp * 4 + 4)
		if not str(target).startswith("$"):
			label = ""
			for temp in sym_table.keys():
				if str(sym_table[temp]) == str(target):
					label = temp
					break
			if label != "":
				label2address[label].add(inst.address)
				return str(inst).replace(" " + inst.get_target_address(), " " + label)

	return str(inst)

def parse(file):
	'''Parses the given file, and outputs an instance of AsmInfo.
	@param file: The file containing the code.
	@return: An instance of AsmInfo class, holding the data parsed out of the 
	given file.
	'''

	# Calls the external assembler to parse the code. The assembler outputs the
	# instructions and the symbol table.
	assembler = subprocess.Popen(("java -cp ./mars Assembler5ccPort " +
								file).split(" "), stdout=subprocess.PIPE)
	assembler.wait()
	out = assembler.communicate(None)[0]
	out = str(out.decode(encoding='utf_8', errors='strict')).splitlines()

	asm = AsmInfo(file, int(out[1]), int(out[0]))  # Offset of text and data blocks

	# Read symbol table.
	label2address = {}  # Temp map for binding labels to addresses that point to them.
	label_count = int(out[2])
	for i in range(3, 2 * label_count + 3, 2):
		label = out[i]
		address = int(out[i + 1])
		if address not in asm.symbol_table.values():
			asm.symbol_table[label] = address
			if address < asm.memory.offset:
				label2address[label] = set()

	# Read instructionss.
	last_source = ""
	temp_instructions = MemoryMap(int(out[1]))
	instruction_count = int(out[2 * label_count + 3])
	for i in range(2 * label_count + 4, 2 * label_count + 4 * instruction_count + 4, 4):
		instruction = Instruction(*re.sub(",", " ", out[i]).split(" "))
		instruction.address = int(out[i + 3])
		instruction = Instruction(*_fix_jump_label(instruction, asm.symbol_table,
												label2address).split(" "))
		instruction.source = out[i + 1]
		if(instruction.source == "") :
			instruction.source = last_source
		else :
			last_source = instruction.source
		instruction.line_no = int(out[i + 2])
		instruction.address = int(out[i + 3])
		temp_instructions.append(instruction)

	# Dummy instruction, holding exit label
	EXIT_LABEL = ""
	flag = True
	while flag :
		flag = False
		EXIT_LABEL += "z"
		for label in asm.symbol_table :
			if label.startswith(EXIT_LABEL) :
				flag = True
				break
	dummy = Instruction("j", EXIT_LABEL)
	dummy.source = ""
	dummy.line_no = temp_instructions[temp_instructions.__len__() - 1].line_no + 1
	dummy.address = temp_instructions[temp_instructions.__len__() - 1].address + 4
	temp_instructions.append(dummy)
	dummy = Instruction("")
	dummy.label = EXIT_LABEL
	dummy.source = ""
	dummy.line_no = temp_instructions[temp_instructions.__len__() - 1].line_no + 1
	dummy.address = temp_instructions[temp_instructions.__len__() - 1].address + 4
	asm.symbol_table[EXIT_LABEL] = dummy.address
	label2address[EXIT_LABEL] = {dummy.address - 4}
	temp_instructions.append(dummy)

	# Bind labels and instructions.
	for sym in asm.symbol_table:
		if asm.symbol_table[sym] < asm.memory.offset:
			temp_instructions[asm.symbol_table[sym]].label = sym

	for label in label2address :
		if label2address[label].__len__() or label == "main":
			asm.label2jumper[label] = { temp_instructions[address] for address in
										label2address[label]}
		else:
			temp_instructions[asm.symbol_table.pop(label)].label = None

	# Read memory map.
	bytes_count = int(out[2 * label_count + 4 * instruction_count + 4])
	for i in range(2 * label_count + 4 * instruction_count + 5,
				   2 * label_count + 4 * instruction_count + bytes_count + 5):
		asm.memory.append(int(out[i]))

	# Restructure the instructions into a linked list
	asm.instructions.root = temp_instructions[0]
	tail = temp_instructions[0]
	if tail.label :
		asm.instructions.labels[tail.label] = tail
	for i in range(1, temp_instructions.__len__()) :
		temp_instructions[i].prev = tail
		tail.next = temp_instructions[i]
		tail = tail.next
		if tail.label :
			asm.instructions.labels[tail.label] = tail
	asm.instructions._size = temp_instructions.__len__()

	if not "main" in asm.symbol_table :
		asm.symbol_table["main"] = asm.instructions.root.address
		asm.label2jumper["main"] = set() 
		asm.instructions.labels["main"] = asm.instructions.root
		asm.instructions.root.label = "main"
		
	return asm
