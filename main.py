'''
Created on Jan 12, 2013

@copyright: 
Copyright (C) Us5ive!
This code is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3, or (at your option)
any later version.

This code is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this code; see the file COPYING.  If not, write to
the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
'''

import sys

from parser.parse import parse
from parser.unparse import unparse
from optimizer.optimize import optimize
from emulator.emulate import emulate

def total_instructions(stats):
	sum_ = 0
	for i in stats:
		sum_ += i
	return sum_

def go(input_, output):
	asm = parse(input_)
	
	stats = emulate(input_)
	print("Befeore:\n#instruction: %d\tcode size: %d Bytes" % 
		(total_instructions(stats), asm.instructions.__len__() * 4 - 4) )
	
	new_asm = optimize(asm)
	unparse(new_asm, output)
	
	stats = emulate(output)
	print("After:\n#instruction: %d\tcode size: %d Bytes\n" % 
		(total_instructions(stats), new_asm.instructions.__len__() * 4 - 4) )

if __name__ == '__main__':
	go(sys.argv[1], sys.argv[2])
