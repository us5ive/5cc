'''
Created on Jan 12, 2013

@copyright: 
Copyright (C) Us5ive!
This code is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3, or (at your option)
any later version.

This code is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this code; see the file COPYING.  If not, write to
the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
'''

import subprocess
import time
import os

def emulate(assembly_file):
	temp_file = "./" + str(time.time())
	os.mknod(temp_file)
	print('############################### Emulator Starts '
		'################################') 
	emulator = subprocess.Popen(("java -cp ./mars Emulator5ccPort " +
								assembly_file + " " + temp_file).split(" "))
	emulator.wait()
	print('\n################################ Emulator Ends '
		  '#################################')
	
	with open(temp_file) as stat_file:
		stats = []
		for line in stat_file:
			stats.append(int(line))
	
	os.remove(temp_file)
	return stats

if __name__ == '__main__':
	pass
