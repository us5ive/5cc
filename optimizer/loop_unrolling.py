'''
Created on Jan 13, 2013

@copyright: 
Copyright (C) Us5ive!
This code is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 3, or (at your option) any later version.

This code is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this code; see the file COPYING.  If not, write to the Free Software Foundation,
675 Mass Ave, Cambridge, MA 02139, USA.
'''

import optimizer.utils.loops
from parser.Instruction import Instruction

_cache1 = {}
_cache2 = {}
_UNKNOWN = {}

def _rec_get_value(var, bb, index) :
	'''Predicts the value of a given variable, on a given instruction, if it's
	possibly predictable.
	@param var: The variable to be predicted.
	@param bb ; The basic block in which the required instruction is.
	@param index : Index of the instruction within the basic block.
	@return : Variable's value on the given instruction, if it could be predicted.
	Or None otherwise.
	'''

	if not bb :
		return 0
	if var == "$0" :
		return 0

	global _UNKNOWN
	global _cache2
	if not var in _cache2 :
		_cache2[var] = {}

	inst = None
	if index >= 0 :
		i = index
		inst = bb[i]

		if inst in _cache2[var] :
			return _cache2[var][inst]
		if not var in _UNKNOWN:
			_UNKNOWN[var] = object()
		_cache2[var][inst] = _UNKNOWN[var]

		temp = inst.writes()
		if temp != None and var in temp :
			if inst.name == "add" or inst.name == "add.s" or inst.name == "addu":
				temp1 = _rec_get_value(inst.args[1], bb, i - 1)
				if temp1 != None and not temp1 in _UNKNOWN.values() :
					temp2 = _rec_get_value(inst.args[2], bb, i - 1)
					if temp2 != None and not temp2 in _UNKNOWN.values():
						_cache2[var][inst] = temp1 + temp2
			elif inst.name == "addi" or inst.name == "addiu" :
				temp1 = _rec_get_value(inst.args[1], bb, i - 1)
				if temp1 != None and not temp1 in _UNKNOWN.values():
					_cache2[var][inst] = temp1 + int(inst.args[2])
			elif inst.name == "and" :
				temp1 = _rec_get_value(inst.args[1], bb, i - 1)
				if temp1 != None and not temp1 in _UNKNOWN.values():
					temp2 = _rec_get_value(inst.args[2], bb, i - 1)
					if temp2 != None and not temp2 in _UNKNOWN.values():
						_cache2[var][inst] = temp1 & temp2
			elif inst.name == "andi" :
				temp1 = _rec_get_value(inst.args[1], bb, i - 1)
				if temp1 != None and not temp1 in _UNKNOWN.values():
					_cache2[var][inst] = temp1 & int(inst.args[2])
			elif inst.name == "div.s" :
				temp2 = _rec_get_value(inst.args[2], bb, i - 1)
				if temp2 != None and not temp2 in _UNKNOWN.values():
					temp1 = _rec_get_value(inst.args[1], bb, i - 1)
					if temp1 != None and not temp1 in _UNKNOWN.values():
						_cache2[var][inst] = temp1 / temp2
			# TODO load
			elif inst.name == "lui" :
				_cache2[var][inst] = int(inst.args[1]) << 32
			elif inst.name == "madd" or inst.name == "maddu" :
				temp0 = _rec_get_value(inst.args[0], bb, i - 1)
				if temp0 != None and not temp0 in _UNKNOWN.values():
					temp1 = _rec_get_value(inst.args[1], bb, i - 1)
					if var == "$lo" :
						temp = _rec_get_value("$lo", bb, i - 1)
						if temp != None and not temp in _UNKNOWN.values():
							_cache2[var][inst] = (temp + temp1 * temp2) & 0xFFFFFFFF
					else :
						temp_lo = _rec_get_value("$lo", bb, i - 1)
						if temp_lo != None and not temp_lo in _UNKNOWN.values():
							temp_hi = _rec_get_value("$lo", bb, i - 1)
							if temp_hi != None and not temp_hi in _UNKNOWN.values():
								_cache2[var][inst] = ((temp_hi << 32) | temp_lo + temp1 * temp2) >> 32
			elif inst.name == "mfc0" :
				_cache2[var][inst] = _rec_get_value("$c0." + inst.args[1][1:], bb, i - 1)
			elif inst.name == "mov.s" :
				_cache2[var][inst] = _rec_get_value(inst.args[1], bb, i - 1)
			elif inst.name == "mfhi" :
				_cache2[var][inst] = _rec_get_value("$hi", bb, i - 1)
			elif inst.name == "mflo" :
				_cache2[var][inst] = _rec_get_value("$lo", bb, i - 1)
			elif inst.name == "movf" or inst.name == "movf.s" :
				if inst.args[2] == None :
					arg = 0
				else :
					arg = int(inst.args[2])
				temp2 = _rec_get_value("$c0." + arg, bb, i - 1)
				if temp2 != None and not temp2 in _UNKNOWN.values():
					_cache2[var][inst] = _rec_get_value(inst.args[1], bb, i - 1)
			elif inst.name == "movn" or inst.name == "movn.s" :
				temp2 = _rec_get_value(inst.args[2], bb, i - 1)
				if temp2 != None and not temp2 in _UNKNOWN.values():
					_cache2[var][inst] = _rec_get_value(inst.args[1], bb, i - 1)
			elif inst.name == "movt" or inst.name == "movt.s" :
				if inst.args[2] == None :
					arg = 0
				else :
					arg = inst.args[2]
				temp2 = _rec_get_value("$c0." + arg, bb, i - 1)
				if temp2 != None and not temp2 in _UNKNOWN.values():
					_cache2[var][inst] = _rec_get_value(inst.args[1], bb, i - 1)
			elif inst.name == "movz" or inst.name == "movz.s" :
				temp2 = _rec_get_value(inst.args[2], bb, i - 1)
				if temp2 != None and not temp2 in _UNKNOWN.values():
					_cache2[var][inst] = _rec_get_value(inst.args[1], bb, i - 1)
			elif inst.name == "msub" or inst.name == "msubu" :
				temp0 = _rec_get_value(inst.args[0], bb, i - 1)
				if temp0 != None and not temp0 in _UNKNOWN.values():
					temp1 = _rec_get_value(inst.args[1], bb, i - 1)
					if var == "$lo" :
						temp = _rec_get_value("$lo", bb, i - 1)
						if temp != None and not temp in _UNKNOWN.values():
							_cache2[var][inst] = (temp - temp1 * temp2) & 0xFFFFFFFF
					else :
						temp_lo = _rec_get_value("$lo", bb, i - 1)
						if temp_lo != None and not temp_lo in _UNKNOWN.values():
							temp_hi = _rec_get_value("$lo", bb, i - 1)
							if temp_hi != None and not temp_hi in _UNKNOWN.values():
								_cache2[var][inst] = ((temp_hi << 32) | temp_lo - \
													 temp1 * temp2) >> 32
			elif inst.name == "mtc0" or inst.name == "mtc1" or \
			inst.name == "mthi" or inst.name == "mtlo":
				_cache2[var][inst] = _rec_get_value(inst.args[0], bb, i - 1)
			elif inst.name == "mul" or inst.name == "mul.s" :
				temp1 = _rec_get_value(inst.args[1], bb, i - 1)
				if temp1 != None and not temp1 in _UNKNOWN.values():
					temp2 = _rec_get_value(inst.args[2], bb, i - 1)
					if temp2 != None and not temp2 in _UNKNOWN.values():
						_cache2[var][inst] = (temp1 * temp2) & 0xFFFFFFFF
			elif inst.name == "mult" or inst.name == "multu" :
				temp1 = _rec_get_value(inst.args[1], bb, i - 1)
				if temp1 != None and not temp1 in _UNKNOWN.values():
					temp2 = _rec_get_value(inst.args[2], bb, i - 1)
					if temp2 != None and not temp2 in _UNKNOWN.values():
						if var == "$lo" :
							_cache2[var][inst] = (temp1 * temp2) & 0xFFFFFFFF
						else :
							_cache2[var][inst] = (temp1 * temp2) >> 32
			elif inst.name == "neg.s" :
				temp1 = _rec_get_value(inst.args[1], bb, i - 1)
				if temp1 != None and not temp1 in _UNKNOWN.values():
					_cache2[var][inst] = -temp1
			elif inst.name == "nor" :
				temp1 = _rec_get_value(inst.args[1], bb, i - 1)
				if temp1 != None and not temp1 in _UNKNOWN.values():
					temp2 = _rec_get_value(inst.args[2], bb, i - 1)
					if temp2 != None and not temp2 in _UNKNOWN.values():
						_cache2[var][inst] = ~(temp1 | temp2)
			elif inst.name == "or" :
				temp1 = _rec_get_value(inst.args[1], bb, i - 1)
				if temp1 != None and temp1[0] == 0:
					temp2 = _rec_get_value(inst.args[2], bb, i - 1)
					if temp2 != None and not temp2 in _UNKNOWN.values():
						_cache2[var][inst] = temp1 | temp2
			elif inst.name == "ori" :
				temp1 = _rec_get_value(inst.args[1], bb, i - 1)
				if temp1 != None and not temp1 in _UNKNOWN.values():
					_cache2[var][inst] = temp1 | int(inst.args[2])
			# TODO store
			elif inst.name == "sra" or inst.name == "srl" :
				temp1 = _rec_get_value(inst.args[1], bb, i - 1)
				if temp1 != None and not temp1 in _UNKNOWN.values():
					_cache2[var][inst] = (temp1 << int(inst.args[2])) & 0xFFFFFFFF
			elif inst.name == "sla" :
				temp1 = _rec_get_value(inst.args[1], bb, i - 1)
				if temp1 != None and not temp1 in _UNKNOWN.values():
					_cache2[var][inst] = (temp1 >> int(inst.args[2])) & 0xFFFFFFFF
			elif inst.name == "sub" or inst.name == "subu" or inst.name == "sub.s" :
				temp1 = _rec_get_value(inst.args[1], bb, i - 1)
				if temp1 != None and not temp1 in _UNKNOWN.values():
					temp2 = _rec_get_value(inst.args[2], bb, i - 1)
					if temp2 != None and not temp2 in _UNKNOWN.values():
						_cache2[var][inst] = temp1 - temp2
			elif inst.name == "syscall" :
				_cache2[var][inst] = None
			elif inst.name == "xor" :
				temp1 = _rec_get_value(inst.args[1], bb, i - 1)
				if temp1 != None and not temp1 in _UNKNOWN.values():
					temp2 = _rec_get_value(inst.args[2], bb, i - 1)
					if temp2 != None and not temp2 in _UNKNOWN.values():
						_cache2[None][inst] = temp1 ^ temp2
			elif inst.name == "xori" :
				temp1 = _rec_get_value(inst.args[1], bb, i - 1)
				if temp1 != None and not temp1 in _UNKNOWN.values():
					_cache2[var][inst] = temp1[1] ^ int(inst.args[2])
			return _cache2[var][inst]
		else :
			if index > 0 :
				_cache2[var][inst] = _rec_get_value(var, bb, index - 1)
				return _cache2[var][inst]

	if bb.first.label == "main" :
		if inst :
			_cache2[var][inst] = 0
		return 0
	else :
		result = None
		for prev in bb.prev :
			if result == None :
				result = _rec_get_value(var, prev, len(prev) - 1)
			else:
				temp = _rec_get_value(var, prev, len(prev) - 1)
				if result != temp and not temp in _UNKNOWN.values() :
					return None
		if inst :
			_cache2[var][inst] = result
		return result

def _get_value(var, bb, index):
	global _UNKNOWN
	temp = _rec_get_value(var, bb, index)
	if not isinstance(temp, int) :
		return None
	if var in _UNKNOWN and temp == _UNKNOWN[var] :
		return None
	return temp

def _get_weights(mem_w, mem_r, bb, index, loop):
	'''Get value of mem_w as a linear function of mem_r on the given instruction,
	if possible.
	@param mem_w: The unknown variable in question.
	@param mem_r: The known variable.
	@param bb: Basic block in which the instruction is.
	@param index: index of the instruction in its basic block.
	@param loop: The loop in which the instruction is.
	@return: A pair in form of [A, B], which means "mem_w == mem_r * A + B". Or
	None if it could not be found. 
	'''

	if mem_w == "$0" :
		return [0, 0]
	if mem_r == "$0" :
		return None

	global _cache1
	if not (mem_w, mem_r) in _cache1 :
		_cache1[(mem_w, mem_r)] = {}

	inst = None
	if index >= 0 :
		i = index
		inst = bb[i]

		if inst in _cache1[(mem_w, mem_r)] :
			return _cache1[(mem_w, mem_r)][inst]
		_cache1[(mem_w, mem_r)][inst] = None

		temp = inst.writes()
		if temp and mem_w in temp :
			if inst.name == "add" or inst.name == "add.s" or inst.name == "addu":
				temp1 = _get_weights(inst.args[1], mem_r, bb, i - 1, loop)
				if temp1 :
					temp2 = _get_weights(inst.args[2], mem_r, bb, i - 1, loop)
					if temp2 :
						_cache1[(mem_w, mem_r)][inst] = [temp1[0] + temp2[0], temp1[1] + temp2[1]]
			elif inst.name == "addi" or inst.name == "addiu" :
				temp1 = _get_weights(inst.args[1], mem_r, bb, i - 1, loop)
				if temp1 :
					_cache1[(mem_w, mem_r)][inst] = [temp1[0], temp1[1] + int(inst.args[2])]
			elif inst.name == "and" :
				temp1 = _get_weights(inst.args[1], mem_r, bb, i - 1, loop)
				if temp1 and temp1[0] == 0:
					temp2 = _get_weights(inst.args[2], mem_r, bb, i - 1, loop)
					if temp2 and temp2[0] == 0:
						_cache1[(mem_w, mem_r)][inst] = [0, temp1[1] & temp2[1]]
			elif inst.name == "andi" :
				temp1 = _get_weights(inst.args[1], mem_r, bb, i - 1, loop)
				if temp1 and temp1[0] == 0:
					_cache1[(mem_w, mem_r)][inst] = [0, temp1[1] & int(inst.args[2])]
			elif inst.name == "div.s" :
				temp2 = _get_weights(inst.args[2], mem_r, bb, i - 1, loop)
				if temp2 and temp2[0] == 0 :
					temp1 = _get_weights(inst.args[1], mem_r, bb, i - 1, loop)
					if temp1 :
						_cache1[(mem_w, mem_r)][inst] = [temp1[0] / temp2[1], temp1[1] / temp2[1]]
			# TODO load
			elif inst.name == "jal" or inst.name == "jalr" or \
			inst.name == "bgezal" or inst.name == "bltzal" :
				_cache1[(mem_w, mem_r)][inst] = None
			elif inst.name == "lui" :
				_cache1[(mem_w, mem_r)][inst] = [0, int(inst.args[1] << 16)]
			elif inst.name == "madd" or inst.name == "maddu" :
				if mem_w == "$lo" :
					temp = _get_weights("$lo", mem_r, bb, i - 1, loop)
					if temp:
						temp0 = _get_weights(inst.args[0], mem_r, bb, i - 1, loop)
						if temp0 :
							temp1 = _get_weights(inst.args[1], mem_r, bb, i - 1, loop)
							if temp0[0] == 0 or temp1[0] == 0 :
								_cache1[(mem_w, mem_r)][inst] = [temp[0] + temp0[0] * temp1[1] + \
								temp0[1] * temp1[0], temp[1] + temp0[1] * temp1[1]]
			elif inst.name == "mfc0" :
				_cache1[(mem_w, mem_r)][inst] = _get_weights("$c0." + inst.args[1][1:], mem_r, bb, i - 1, loop)
			elif inst.name == "mov.s" :
				_cache1[(mem_w, mem_r)][inst] = _get_weights(inst.args[1], mem_r, bb, i - 1, loop)
			elif inst.name == "mfhi" :
				_cache1[(mem_w, mem_r)][inst] = _get_weights("$hi", mem_r, bb, i - 1, loop)
			elif inst.name == "mflo" :
				_cache1[(mem_w, mem_r)][inst] = _get_weights("$lo", mem_r, bb, i - 1, loop)
			elif inst.name == "movf" or inst.name == "movf.s" :
				if inst.args[2] == None :
					arg = 0
				else :
					arg = int(inst.args[2])
				temp2 = _get_weights("$c0." + arg, mem_r, bb, i - 1, loop)
				if temp2 and temp2[0] == 0 and temp2[1] == 0:
					_cache1[(mem_w, mem_r)][inst] = _get_weights(inst.args[1], mem_r, bb, i - 1, loop)
			elif inst.name == "movn" or inst.name == "movn.s" :
				temp2 = _get_weights(inst.args[2], mem_r, bb, i - 1, loop)
				if temp2 and temp2[0] == 0 and temp2[1] != 0 :
					_cache1[(mem_w, mem_r)][inst] = _get_weights(inst.args[1], mem_r, bb, i - 1, loop)
			elif inst.name == "movt" or inst.name == "movt.s" :
				if inst.args[2] == None :
					arg = 0
				else :
					arg = inst.args[2]
				temp2 = _get_weights("$c0." + arg, mem_r, bb, i - 1, loop)
				if temp2 and temp2[0] == 0 and temp2[1] != 0:
					_cache1[(mem_w, mem_r)][inst] = _get_weights(inst.args[1], mem_r, bb, i - 1, loop)
			elif inst.name == "movz" or inst.name == "movz.s" :
				temp2 = _get_weights(inst.args[2], mem_r, bb, i - 1, loop)
				if temp2 and temp2[0] == 0 and temp2[1] == 0 :
					_cache1[(mem_w, mem_r)][inst] = _get_weights(inst.args[1], mem_r, bb, i - 1, loop)
			elif inst.name == "msub" or inst.name == "msubu" :
				if mem_w == "$lo" :
					temp = _get_weights("$lo", mem_r, bb, i - 1, loop)
					if temp:
						temp0 = _get_weights(inst.args[0], mem_r, bb, i - 1, loop)
						if temp0 :
							temp1 = _get_weights(inst.args[1], mem_r, bb, i - 1, loop)
							if temp0[0] == 0 or temp1[0] == 0 :
								_cache1[(mem_w, mem_r)][inst] = [temp[0] - temp0[0] * temp1[1] - \
								temp0[1] * temp1[0], temp[1] - temp0[1] * temp1[1]]
			elif inst.name == "mtc0" or inst.name == "mtc1" or \
			inst.name == "mthi" or inst.name == "mtlo":
				_cache1[(mem_w, mem_r)][inst] = _get_weights(inst.args[0], mem_r, bb, i - 1, loop)
			elif inst.name == "mul" or inst.name == "mul.s" :
				temp1 = _get_weights(inst.args[1], mem_r, bb, i - 1, loop)
				if temp1 :
					temp2 = _get_weights(inst.args[2], mem_r, bb, i - 1, loop)
					if temp2 and (temp1[0] == 0 or temp2[0] == 0):
						_cache1[(mem_w, mem_r)][inst] = [temp1[0] * temp2[1] + temp1[1] * temp2[0], \
							temp1[1] * temp2[1]]
			elif inst.name == "mult" or inst.name == "multu" :
				if mem_w == "$lo" :
					temp1 = _get_weights(inst.args[1], mem_r, bb, i - 1, loop)
					if temp1 :
						temp2 = _get_weights(inst.args[2], mem_r, bb, i - 1, loop)
						if temp1[0] == 0 or temp2[0] == 0 :
							_cache1[(mem_w, mem_r)][inst] = [temp1[0] * temp2[1] + temp1[1] * temp2[0],
								temp1[1] * temp2[1]]
			elif inst.name == "neg.s" :
				temp1 = _get_weights(inst.args[1], mem_r, bb, i - 1, loop)
				if temp1 :
					_cache1[(mem_w, mem_r)][inst] = [-temp1[0], -temp1[1]]
			elif inst.name == "nor" :
				temp1 = _get_weights(inst.args[1], mem_r, bb, i - 1, loop)
				if temp1 and temp1[0] == 0:
					temp2 = _get_weights(inst.args[2], mem_r, bb, i - 1, loop)
					if temp2 and temp2[0] == 0:
						_cache1[(mem_w, mem_r)][inst] = [0, ~(temp1[1] | temp2[1])]
			elif inst.name == "or" :
				temp1 = _get_weights(inst.args[1], mem_r, bb, i - 1, loop)
				if temp1 and temp1[0] == 0:
					temp2 = _get_weights(inst.args[2], mem_r, bb, i - 1, loop)
					if temp2 and temp2[0] == 0:
						_cache1[(mem_w, mem_r)][inst] = [0, temp1[1] | temp2[1]]
			elif inst.name == "ori" :
				temp1 = _get_weights(inst.args[1], mem_r, bb, i - 1, loop)
				if temp1 and temp1[0] == 0:
					_cache1[(mem_w, mem_r)][inst] = [0, temp1[1] | int(inst.args[2])]
			# TODO store
			elif inst.name == "sra" or inst.name == "srl" :
				temp1 = _get_weights(inst.args[1], mem_r, bb, i - 1, loop)
				if temp1 :
					_cache1[(mem_w, mem_r)][inst] = [temp1[0] << int(inst.args[2]), temp1[1] << int(inst.args[2])]
			elif inst.name == "sla" :
				temp1 = _get_weights(inst.args[1], mem_r, bb, i - 1, loop)
				if temp1 :
					_cache1[(mem_w, mem_r)][inst] = [temp1[0] >> int(inst.args[2]), temp1[1] >> int(inst.args[2])]
			elif inst.name == "sub" or inst.name == "subu" or inst.name == "sub.s" :
				temp1 = _get_weights(inst.args[1], mem_r, bb, i - 1, loop)
				if temp1 :
					temp2 = _get_weights(inst.args[2], mem_r, bb, i - 1, loop)
					if temp2 :
						_cache1[(mem_w, mem_r)][inst] = [temp1[0] - temp2[0], temp1[1] - temp2[1]]
			elif inst.name == "syscall" :
				_cache1[(mem_w, mem_r)][inst] = None
			elif inst.name == "xor" :
				temp1 = _get_weights(inst.args[1], mem_r, bb, i - 1, loop)
				if temp1 and temp1[0] == 0:
					temp2 = _get_weights(inst.args[2], mem_r, bb, i - 1, loop)
					if temp2 and temp2[0] == 0:
						_cache1[(mem_w, mem_r)][inst] = [0, temp1[1] ^ temp2[1]]
			elif inst.name == "xori" :
				temp1 = _get_weights(inst.args[1], mem_r, bb, i - 1, loop)
				if temp1 and temp1[0] == 0:
					_cache1[(mem_w, mem_r)][inst] = [0, temp1[1] ^ int(inst.args[2])]
			return _cache1[(mem_w, mem_r)][inst]
		else :
			if index > 0 :
				_cache1[(mem_w, mem_r)][inst] = _get_weights(mem_w, mem_r, bb, index - 1, loop)
				return _cache1[(mem_w, mem_r)][inst]

	loop_size = 0
	for block in loop :
		for line in block :
			loop_size += 1
	if len(_cache1[(mem_w, mem_r)]) == loop_size :
		if mem_w == mem_r :
			if inst :
				_cache1[(mem_w, mem_r)][inst] = [1, 0]
			return [1, 0]
		else :
			result = None
			for pre_loop in optimizer.utils.loops.get_entry(loop).prev :
				temp = _get_value(mem_w, pre_loop, len(pre_loop) - 1)
				if temp and (temp == result or result == None) :
					result = temp
				else :
					return None
			if inst :
				_cache1[(mem_w, mem_r)][inst] = [0, result]
			return [0, result]
	else :
		result = None
		for prev in bb.prev :
			if prev in loop :
				if not result :
					result = _get_weights(mem_w, mem_r, prev, len(prev) - 1, loop)
				elif result != _get_weights(mem_w, mem_r, prev, len(prev) - 1, loop) :
					return None
		if inst :
			_cache1[(mem_w, mem_r)][inst] = result
		return result

def _get_counter_info(loop):
	'''Gets the information about the loop's counter.'''

	candidates = []
	entry = optimizer.utils.loops.get_entry(loop)

	for exit_ in optimizer.utils.loops.get_exits(loop) :
		gaurds = []
		for gaurd in exit_.last.reads() :
			weight = _get_weights(gaurd, gaurd, exit_, len(exit_) - 1, loop)
			if weight :
				if entry.first.label == "main" :
					initial = 0
				else :
					initial = None
					for prev in entry.prev :
						if not prev in loop :
							if initial == None :
								initial = _get_value(gaurd, prev, len(prev) - 1)
							elif initial != _get_value(gaurd, prev, len(prev) - 1) :
								initial = None
								break
					gaurds.append((gaurd, weight, initial))
			else :
				break
		else:
			candidates.append((exit_, gaurds))

	for c in candidates :
		exit_ = c[0]
		gaurds = c[1]
		for gaurd in gaurds :
			if gaurd[2] == None :
				break
		else :
			return c

	for c in candidates :
		exit_ = c[0]
		gaurds = c[1]
		for gaurd in gaurds :
			if gaurd[1][0] != 0 and gaurd[1][0] != 1 and gaurd[1][1] != 0 :
				break
		else :
			return c
	return None

def _count_static_loop_iteration(branch, entry_name, gaurds):
	'''Determines the number of iterations a loop has to make before termination.
	@param branch : The branch instruction that is known to end the loop.
	@param entry_name : Label that indicates the loop header.
	@param gaurds : (sorry for typo) variables, controlling the branch instruction.
	@return : Number of iterations the loop has to make before it exits from the
	specified branch instruction. Or None if it could not be determined.
	'''
	n = 1
	if branch.name == "beq" or branch.name == "bne" :
		weight = [gaurds[0][1], gaurds[1][1]]
		initial = [gaurds[0][2], gaurds[1][2]]
	elif branch.name == "bgez" or branch.name == "bgtz" or\
	branch.name == "blez" or branch.name == "bltz" :
		weight = [gaurds[0][1], [1, 0]]
		initial = [gaurds[0][2], 0]
	else :
		return

	if branch.get_target_address() == entry_name :
		branch_name = {'beq':'bne', 'bne':'beq', 'bgez':'bltz', 'bltz':'bgez',
					'bgtz':'blez', 'blez':'bgtz'}[branch.name]
	else :
		branch_name = branch.name

	g = [initial[0] * weight[0][0] + weight[0][1], initial[1] * weight[1][0] + weight[1][1]]
	if branch_name == "beq" :
		while g[0] != g[1] :
			g[0] = g[0] * weight[0][0] + weight[0][1]
			g[1] = g[1] * weight[1][0] + weight[1][1]
			n += 1
			if n > 1000000000:
				return None
	elif branch_name == "bne" :
		while g[0] == g[1] :
			g[0] = g[0] * weight[0][0] + weight[0][1]
			g[1] = g[1] * weight[1][0] + weight[1][1]
			n += 1
			if n > 1000000000:
				return None
	elif branch_name == "bqez" :
		while g[0] < g[1] :
			g[0] = g[0] * weight[0][0] + weight[0][1]
			g[1] = g[1] * weight[1][0] + weight[1][1]
			n += 1
			if n > 1000000000:
				return None
	elif branch_name == "bgtz" :
		while g[0] <= g[1] :
			g[0] = g[0] * weight[0][0] + weight[0][1]
			g[1] = g[1] * weight[1][0] + weight[1][1]
			n += 1
			if n > 1000000000:
				return None
	elif branch_name == "blez" :
		while g[0] > g[1] :
			g[0] = g[0] * weight[0][0] + weight[0][1]
			g[1] = g[1] * weight[1][0] + weight[1][1]
			n += 1
			if n > 1000000000:
				return None
	elif branch_name == "bltz" :
		while g[0] >= g[1] :
			g[0] = g[0] * weight[0][0] + weight[0][1]
			g[1] = g[1] * weight[1][0] + weight[1][1]
			n += 1
			if n > 1000000000:
				return None

	return n


def _static_unroll(asm, loop, exit_, gaurds, count):
	'''Unrolls a single loop, when the number of iterations is known beforehand.
	@param asm : AsmInfo instance, representing the assembly file.
	@param loop : The loop to be unrolled. A loop is a set of basic blocks.
	@param exit_ : A loop might have several exiting points. This is the basic
	block, that is known to end the loop.
	@param gaurds : (Sorry for the typo, It should have been guards). The
	variable, or variables, that control the termination of the loop.
	@param count : Number of iterations, the loop is predicted to make.
	'''

	hold = []
	min_address = 2147843647
	max_address = -1
	for bb in loop :
		for inst in bb :
			if inst.address < min_address :
				min_address = inst.address
			if inst.address > max_address :
				max_address = inst.address

	inst = asm.instructions[min_address]
	for i in range(min_address, max_address + 4, 4) :
		if not optimizer.utils.loops.is_inst_in_loop(inst, loop) :
			hold.append(inst)
		inst = inst.next

	to_be_inserted = _get_static_unroll_inst_list(asm, loop, exit_, gaurds, count)

	dummy = Instruction("nop")
	asm.insert_instructions([dummy], max_address + 4)
	asm.remove_instructions(min_address, max_address + 4)
	asm.insert_instructions(hold, min_address + 4)
	asm.put_label(dummy.address, asm.get_new_label("dummy"))
	asm.insert_instructions(to_be_inserted, min_address + hold.__len__() * 4)
	asm.remove_instruction(dummy.address)
	asm.prune_labels()

def _get_static_unroll_inst_list(asm, loop, exit_, gaurds, count):
	'''Make the list of instructions that should replace the original loop.'''

	clones = []
	entry_label = optimizer.utils.loops.get_entry(loop).first.label
	for bb in loop :
		for inst in bb :
			clones.append((inst, inst.copy()))
	for i in range(clones.__len__() - 1) :
		min_ind = i
		min_ = clones[min_ind][1].address
		for j in range(i, clones.__len__()) :
			if clones[j][1].address < min_ :
				min_ = clones[j][1].address
				min_ind = j
		temp = clones[min_ind]
		clones[min_ind] = clones[i]
		clones[i] = temp

	for i in range(0, clones.__len__()) :
		if clones[i][0] == exit_.last :
			inst_list = [ pair[1] for pair in clones[0:i] ]
			for pair in clones[i:] :
				new_inst = Instruction("nop")
				new_inst.label = pair[1].label
				inst_list.append(new_inst)
			break

	for i in range(0, clones.__len__()) :
		pair = clones[i]
		if pair[0].is_brancher() :
			if pair[0] == exit_.last :
				new_inst = Instruction("nop")
				new_inst.label = exit_.last.label
				clones[i] = (pair[0], new_inst)
			elif not optimizer.utils.loops.is_inst_in_loop(pair[0].next, loop) and clones[i][0] != exit_.last :
				target = pair[0].get_target_address()
				clones[i] = (pair[0], Instruction("j", target))
			elif not optimizer.utils.loops.is_inst_in_loop(asm.instructions.labels[pair[0].get_target_address()], loop) :
				new_inst = Instruction("nop")
				new_inst.label = pair[0].label
				clones[i] = (pair[0], new_inst)

	temp2 = []
	labels_to_change = set()
	for pair in clones :
		if pair[1] :
			temp2.append(pair[1])
			if pair[1].label :
				labels_to_change.add(pair[1].label)
	labels_to_change.remove(entry_label)

	for i in range(1, count) :
		yet_another_copy = [ inst.copy() for inst in temp2 ]
		for k in range(0, yet_another_copy.__len__()) :
			inst = yet_another_copy[k]
			if inst.get_target_address() :
				for j in range(0, 3) :
					if inst.args[j] == entry_label :
						if i == count - 1 :
							yet_another_copy[k] = Instruction("nop")
						else :
							yet_another_copy[k].args[j] += "_" + str(i + 1)
					if inst.args[j] in labels_to_change :
						yet_another_copy[k].args[j] += "_" + str(i)
			if inst.label and (inst.label in labels_to_change or inst.label == entry_label) :
				yet_another_copy[k].label += "_" + str(i)
		inst_list[inst_list.__len__():] = yet_another_copy

	return inst_list

def _dynamic_unroll(asm, loop, exit_, guards) :
	print("dynamic unrolling is not implemented yet.")
	pass

def _unroll(asm, loop):
	'''Unrolls a given loop if possible.
	@param loop: Loop to be unrolled.
	'''

	entry_name = optimizer.utils.loops.get_entry(loop).first.label
	counter_inf = _get_counter_info(loop)
	if counter_inf :
		exit_ = counter_inf[0]
		gaurds = counter_inf[1]

		min_iteration = 2147483647
		static_unroll_params = None
		for gaurd in gaurds :
			if gaurd[2] == None :
				break
		else :
			count = _count_static_loop_iteration(exit_.last, entry_name, gaurds)
			if count != None and count < min_iteration :
				min_iteration = count
				static_unroll_params = (exit_, gaurds, count)
			if static_unroll_params :
				_static_unroll(asm, loop, *static_unroll_params)

def loop_unroll(asm):
	'''Unrolls a loop is the assembly code, if possible.
	@param asm : AsmInfo instance, representing the code.
	'''

	global _cache1
	global _cache2
	_cache1 = {}
	_cache2 = {}
	loops = optimizer.utils.loops.get_loops(asm)

	while len(loops) :
		biggest_loop = loops[0]
		for loop in loops :
			if len(loop) > len(biggest_loop) :
				biggest_loop = loop

		_unroll(asm, loop)
		loops.remove(loop)
