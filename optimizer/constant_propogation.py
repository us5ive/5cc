'''
Created on Jan 13, 2013

@copyright: 
Copyright (C) Us5ive!
This code is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 3, or (at your option) any later version.

This code is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this code; see the file COPYING.  If not, write to the Free Software Foundation,
675 Mass Ave, Cambridge, MA 02139, USA.
'''
from optimizer.utils.BasicBlock import *
import optimizer.loop_unrolling
import optimizer.utils.loops
from parser.Instruction import Instruction
from optimizer.utils.DataflowAnalysis import *
import math

def _is_pow2(x):
	if (int)(math.log(x) / math.log(2)) - (math.log(x) / math.log(2)) == 0:
		return True
	return False
def constant_propogation(asm):
	bbs = get_basic_blocks(asm)
	iterative_RD(asm)

	regs = []
	for i in xrange(32):
		s = "$"+str(i)
		regs.append(s)


	for bb in bbs:
		node = bb.first
		i = 0
		while node != bb.last.next:
			if node.writes() != None:
				if len(node.writes()) == 1:
					var = node.writes()[0]
					if var in regs:
						if optimizer.loop_unrolling._get_value(var, bb, i) != None:
							asm.change_instruction(node.address, "li", var, optimizer.loop_unrolling._get_value(var, bb, i))
							i += 1
							node = node.next
							continue
			
			know_vars = []
			unknown_vars = []
			status_vars = []
			if node.reads() != None:
				for var in node.reads():
					if optimizer.loop_unrolling._get_value(var, bb, i) != None:
						status_vars.append("know")
						know_vars.append(optimizer.loop_unrolling._get_value(var, bb, i))
					else:
						if var != node.args[0]:
							status_vars.append("unknown")
							unknown_vars.append(var)
						else:
							if optimizer.loop_unrolling._get_value(var, bb, i - 1) != None:
								status_vars.append("know")
								know_vars.append(optimizer.loop_unrolling._get_value(var, bb, i - 1))
							
			if len(know_vars) > 0:
				if node.name == "add":
					asm.change_instruction(node.address, "addi", node.args[0], unknown_vars[0], know_vars[0])
				elif node.name == "addu":
					asm.change_instruction(node.address, "addiu", node.args[0], unknown_vars[0], know_vars[0])
				elif node.name == "and":
					asm.change_instruction(node.address, "andi", node.args[0], unknown_vars[0], know_vars[0])
				elif node.name == "or":
					asm.change_instruction(node.address, "ori", node.args[0], unknown_vars[0], know_vars[0])
				elif node.name == "xor":
					asm.change_instruction(node.address, "xori", node.args[0], unknown_vars[0], know_vars[0])
				elif node.name == "sllv":
					asm.change_instruction(node.address, "sll", node.args[0], unknown_vars[0], know_vars[0])
				elif node.name == "srlv":
					asm.change_instruction(node.address, "srl", node.args[0], unknown_vars[0], know_vars[0])
				elif node.name == "mul":
					if _is_pow2(know_vars[0]):
						print len(unknown_vars)
						asm.change_instruction(node.address, "sll", node.args[0], unknown_vars[0], (int)(math.log(know_vars[0]) / math.log(2)))

			i += 1
			node = node.next
