'''
Created on Jan 14, 2013

@copyright: 
Copyright (C) Us5ive!
This code is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3, or (at your option)
any later version.

This code is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this code; see the file COPYING.  If not, write to
the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
'''

import parser.Instruction

def _is_unconditional_jump_wo_return(asm, jumper):
	if jumper.name in parser.Instruction.UNCONDITIONAL_JUMPERS_WO_RETURN: #or jumper.name is "syscall" and predict_reg_value(asm, jumper.address, 2) == 10 :
		return jumper
	return False

def _get_unconditional_jumper_wo_return(asm, label):
	'''Gets one of the unconditional jumps without return instructions, pointed
	to this label.
	@param label: The label.
	@return: One of the unconditional jumps without return instructions, pointed
	to this label.
	'''

	for jumper in asm.label2jumper[label] :
		if _is_unconditional_jump_wo_return(asm, jumper) and \
		jumper.address < asm.instructions.__len__() * 4 + asm.instructions.offset - 8:
			return jumper
	return None

def jump_eliminate(asm):
	'''
	Eliminates unnecessary jumps by moving the target to right after the jump
	instruction and removing the unnecessary jump.
	Example:
	
	Before:			After:
	Inst0			Inst0
	j L1			L1: Inst2
	L2: Inst1		j L4
	j L3			L2: Inst1
	L1: Inst2		j L3
	j L4
	'''
	
	for label in asm.symbol_table.copy() :
		if label in asm.symbol_table :
			address = asm.symbol_table[label]
			if address < asm.memory.offset :
				if address == asm.instructions.offset or _is_unconditional_jump_wo_return(asm, asm.instructions[address - 4]) :
					unconditional_jumper_wo_return = _get_unconditional_jumper_wo_return(asm, label)
					if unconditional_jumper_wo_return :
						unconditional_jumper_wo_return = unconditional_jumper_wo_return.address
						src_start = address
						dest = (unconditional_jumper_wo_return + 4 -
							asm.instructions.offset) // 4
						src_end = asm.instructions.__len__() * 4 + asm.instructions.offset - 4
						for adrs in range(src_start + 4, asm.instructions.__len__() *
										4 + asm.instructions.offset, 4) :
							if asm.instructions[adrs].name in parser.Instruction.UNCONDITIONAL_JUMPERS_WO_RETURN:
								src_end = adrs + 4
								break
						asm.move_instructions(src_start, src_end, dest)
						asm.remove_instruction(_get_unconditional_jumper_wo_return(asm, label).address)
	
	for inst in asm.instructions :
		if _is_unconditional_jump_wo_return(asm, inst) or inst.is_brancher() :
			target = inst.get_target_address()
			if target and not target.startswith('$') and asm.symbol_table[target] == inst.address + 4 :
				inst.name = "nop"
				inst.args = ["","",""]
				asm.label2jumper[target].remove(inst)
				continue
