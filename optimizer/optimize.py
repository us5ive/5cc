'''
Created on Jan 12, 2013

@copyright: 
Copyright (C) Us5ive!
This code is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3, or (at your option)
any later version.

This code is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this code; see the file COPYING.  If not, write to
the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
'''

def clean_up(asm):
	'''Cleans up the synthesized code after optimizations performed.
	@param asm: AsmInfo instance, holding code information.
	'''
	
	ptr = asm.instructions.root
	while ptr :
		temp = ptr.next
		if ptr.name == "nop" :
			asm.remove_instruction(ptr.address)
		ptr = temp
	asm.prune_labels()

def optimize(asm):
	'''Applies the optimization algorithms to the given AsmInfo instance.
	Note: This does not alter the original AsmInfo instance, but takes a deep 
	copy and keeps the original instance unchanged.
	'''
	
	asm = asm.copy()
	# call onto your algorithm here.
	
	return asm
