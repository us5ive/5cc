'''
Created on Jan 17, 2013

@copyright: 
Copyright (C) Us5ive!
This code is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 3, or (at your option) any later version.

This code is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this code; see the file COPYING.  If not, write to the Free Software Foundation,
675 Mass Ave, Cambridge, MA 02139, USA.
'''

from collections import deque
import optimizer.utils.BasicBlock

_label2bb = {}
_component_count = -1

def _dfs1(bb):
	'''Mark a spanning tree, rooted at the given node.
	@param bb: Basic block to be the root.
	'''

	global _component_count
	bb.component = _component_count
	for i in range(0, len(bb.next)) :
		if bb.next[i].component == -1 :
			bb.mark_edge[i] = True
			bb.next[i].par = bb
			_dfs1(bb.next[i])

def _is_reachable(root, node, ignore):
	'''Determines whether or not a given node is reachable from a given root,
	while the path does not pass through a specific vertex.
	@param root: The root.
	@param node: The node.
	@param ignore : The vertex that should be omitted from pass.
	@return: Whether or not node is reachable from root.
	'''

	s = set()
	q = deque()
	q.appendleft(root)
	while(q.__len__()) :
		x = q.pop()
		if x == node :
			return True
		if not x in s and x != ignore :
			s.add(x)
			for next_ in x.next :
				q.appendleft(next_)
	return False

def _extract_loop(bb, i):
	'''Given there is a back-edge between the given basic block and its i-th
	successor, build the natural loop them both.
	@param bb: Basic block.
	@param i: index of the successor.
	@return: A set containing basic blocks of code that fall within this loop.
	'''

	s = set()

	q = deque()
	q.appendleft(bb.next[i])
	while(q.__len__()) :
		x = q.pop()
		if x != bb and not x in s and bb.next[i] in x.dominator and \
		(x == bb.next[i] or _is_reachable(x, bb, bb.next[i])):
			s.add(x)
			for next_ in x.next :
				q.appendleft(next_)
	s.add(bb)
	s.add(bb.next[i])

	return s

def get_loops(asm, bbs=None):
	'''Identifies loops in the control flow graph.
	@param asm: AsmInfo instance.
	@param bbs : List of basic blocks. It will be calculated if you don't provide
	this argument, thus, can be ignored.
	@return: A list of loops. Each loop is itself expressed as a list of basic
	blocks.
	'''

	global _label2bb
	_label2bb = {}
	result = []

	# Get basic blocks
	if bbs == None :
		bbs = optimizer.utils.BasicBlock.get_basic_blocks(asm)
	for bb in bbs :
		if bb.first.label :
			_label2bb[bb.first.label] = bb

	# Initialize DFS
	for bb in bbs :
		bb.mark_edge = [ False for each_next in bb.next ]
		bb.component = -1
		bb.dominator = set()

	# DFS
	global _component_count
	_component_count = 0
	main = _label2bb["main"];
	_dfs1(main)
	for bb in _label2bb.values() :
		if bb.component == -1 :
			_component_count += 1
			_dfs1(bb)

	# Build dominance relation
	for bb in bbs :
		for temp in bbs :
			bb.dominator.add(temp)
	for proc_name in optimizer.utils.procedures.get_procedures(asm) :
		proc_head = _label2bb[proc_name]
		proc_head.dominator = {proc_head}
	for i in range(0, bbs.__len__() * bbs.__len__()) :
		for bb in bbs :
			for prev in bb.prev :
				bb.dominator = bb.dominator.intersection(prev.dominator)
			bb.dominator.add(bb)

	# Inspect back-edges
	for bb in bbs :
		for i in range(0, bb.next.__len__()) :
			if not bb.mark_edge[i] and bb.component == bb.next[i].component and\
			bb.component != -1 and bb.next[i] in bb.dominator:
				result.append(_extract_loop(bb, i))

	return result

def get_entry(loop):
	'''Gets the entry block of the loop.
	@param loop : Loop.
	@return : Entry block of loop.
	'''

	for bb in loop :
		if bb.first.label == "main" :
			return bb
		for prev in bb.prev :
			if not prev in loop :
				return bb
	for bb in loop :
		return bb

def get_exits(loop):
	'''Gets the exit blocks of the loop.
	@param loop : Loop.
	@return : Set exiting blocks
	'''

	result = set()
	for bb in loop :
		for next_ in bb.next :
			if not next_ in loop :
				result.add(bb)
				break

	return result

def is_inst_in_loop(inst, loop):
	'''Determines whether or not the given instruction is inside the given loop.
	@param inst : The instruction.
	@param loop : The loop.
	@return : Whether or not the given instruction is inside the given loop.
	'''

	for bb in loop:
		if inst in bb :
			return True
	return False

def intra_loop_dominance_relation(loop):
	'''Builds the dominance relation between basic blocks of a loop.
	@param loop : Set of basic blocks in the loop.
	@return : a map from each basic block to its set of dominators.
	'''
	
	result = {}
	
	for bb in loop :
		result[bb] = set()
		for temp in loop :
			result[bb].add(temp)
	head = get_entry(loop)
	result[head] = set()
	result[head].add(head)
	for i in range(0, loop.__len__() * loop.__len__()) :
		for bb in loop :
			for prev in bb.prev :
				if prev in loop :
					result[bb] = result[bb].intersection(result[prev])
			result[bb].add(bb)
			
	return result
