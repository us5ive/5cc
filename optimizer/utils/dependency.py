'''
Created on Jan 25, 2013

@copyright: 
Copyright (C) Us5ive!
This code is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3, or (at your option)
any later version.

This code is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this code; see the file COPYING.  If not, write to
the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
'''

from optimizer.utils.BasicBlock import get_basic_blocks
from optimizer.utils.procedures import get_procedures

_proc_headers = None
_bbs = None
_cache_last_write = {}
_cache_first_write = {}
_UNKNOWN = {}


def get_last_write(inst, bb, var):
	'''For a given instruction and a given variable, recursively finds the last
	locations where a write on that variable occurred.
	@param inst : The given instruction.
	@param bb : The basic block that instruction is in.
	@param var : The variable in question.
	@param excluding : Whether the instruction itself should be excluded or not.
	@return : set of instructions all of which are ancestors of the given
	instruction in the CFG, and there is no other write on the given variable,
	on their path to the given instruction. 
	'''

	if var == "$0" :
		return set()

	global _UNKNONW
	global _cache_last_write
	if not var in _cache_last_write :
		_cache_last_write[var] = {}
	result = _rec_get_last_write(inst, bb, var, True)
	for inst in _cache_last_write[var].copy() :
		_cache_last_write[var].pop(inst)
	if result == _UNKNOWN[var] :
		return None
	return result

def _rec_get_last_write(inst, bb, var, excluding):
	'''For a given instruction and a given variable, recursively finds the last
	locations where a write on that variable occurred.
	NOTE: Do not use this function. use get_last_write from this module.
	@param inst : The given instruction.
	@param bb : The basic block that instruction is in.
	@param var : The variable in question.
	@param excluding : Whether the instruction itself should be excluded or not.
	@return : set of instructions all of which are ancestors of the given
	instruction in the CFG, and there is no other write on the given variable,
	on their path to the given instruction. 
	'''

	global _UNKNOWN
	global _cache_last_write
	if not var in _UNKNOWN :
		_UNKNOWN[var] = object()

	if not excluding :
		if not var in _cache_last_write :
			_cache_last_write[var] = {}
		if inst in _cache_last_write[var] :
			return  _cache_last_write[var][inst]
		_cache_last_write[var][inst] = _UNKNOWN[var]
		if inst.writes() and var in inst.writes() :
			_cache_last_write[var][inst] = {inst}
			return _cache_last_write[var][inst]

	if inst == bb.first :
		if inst in _proc_headers :
			_cache_last_write[var][inst] = set()
			return _cache_last_write[var][inst]
		result = set()
		for prev in bb.prev :
			temp = _rec_get_last_write(prev.last, prev, var, False)
			if temp == _UNKNOWN[var] :
				temp = set()
			result = result.union(temp)
		_cache_last_write[var][inst] = result
		return _cache_last_write[var][inst]
	else :
		_cache_last_write[var][inst] = _rec_get_last_write(inst.prev, bb, var, False)
		return _cache_last_write[var][inst]

def get_first_write(inst, bb, var):
	'''For a given instruction and a given variable, recursively finds the first
	locations where a write on that variable occurred.
	@param inst : The given instruction.
	@param bb : The basic block that instruction is in.
	@param var : The variable in question.
	@param excluding : Whether the instruction itself should be excluded or not.
	@return : set of instructions all of which are descendants of the given
	instruction in the CFG, and there is no other write on the given variable,
	on their path from the given instruction. 
	'''

	if var == "$0" :
		return set()

	global _UNKNONW
	global _cache_first_write
	if not var in _cache_first_write :
		_cache_first_write[var] = {}
	result = _rec_get_first_write(inst, bb, var, True)
	for inst in _cache_first_write[var].copy() :
		_cache_first_write[var].pop(inst)
	if result == _UNKNOWN[var] :
		return None
	return result

def _rec_get_first_write(inst, bb, var, excluding):
	'''For a given instruction and a given variable, recursively finds the first
	locations where a write on that variable occurred.
	NOTE: Do not use this function. use get_last_write from this module.
	@param inst : The given instruction.
	@param bb : The basic block that instruction is in.
	@param var : The variable in question.
	@param excluding : Whether the instruction itself should be excluded or not.
	@return : set of instructions all of which are descendants of the given
	instruction in the CFG, and there is no other write on the given variable,
	on their path from the given instruction.  
	'''

	global _UNKNOWN
	global _cache_first_write
	if not var in _UNKNOWN :
		_UNKNOWN[var] = object()

	if not excluding :
		if inst in _cache_first_write[var] :
			return  _cache_first_write[var][inst]
		_cache_first_write[var][inst] = _UNKNOWN[var]
		if inst.writes() and var in inst.writes() :
			_cache_first_write[var][inst] = {inst}
			return _cache_first_write[var][inst]

	if inst == bb.last :
		if inst in _proc_headers :
			_cache_first_write[var][inst] = set()
			return _cache_first_write[var][inst]
		result = set()
		for next in bb.next :
			temp = _rec_get_first_write(next.first, next, var, False)
			if temp == _UNKNOWN[var] :
				temp = set()
			result = result.union(temp)
		_cache_first_write[var][inst] = result
		return _cache_first_write[var][inst]
	else :
		_cache_first_write[var][inst] = _rec_get_first_write(inst.next, bb, var, False)
		return _cache_first_write[var][inst]

def build_true_dependency_graph(asm):
	'''Builds the graph of true dependency relation. We say that statement B
	depends on statement A, when A generates the value used by B.
	@param asm : AsmInfo instance, holding the data about the given assembly code.
	@return : A map from instructions to the (next, prev) tuple, where next and
	prev are the set of descendants and ancestors respectively.
	'''

	result = {}

	for bb in _bbs :
		for inst in bb :
			result[inst] = (set(), set())

	for bb in _bbs :
		for inst in bb :
			if inst.reads() :
				for var in inst.reads() :
					if var.startswith("$") :
						for true_source in get_last_write(inst, bb, var):
							result[inst][0].add(true_source)
							result[true_source][1].add(inst)
					else :
						print("#TODO: memory dependency")  # TODO memroy
	return result

def build_anti_dependency_graph(asm):
	'''Builds the graph of anti dependency relation. We say that statement B
	anti depends on statement A, when A reads the value used by B.
	@param asm : AsmInfo instance, holding the data about the given assembly code.
	@return : A map from instructions to the (next, prev) tuple, where next and
	prev are the set of descendants and ancestors respectively.
	'''

	result = {}

	for bb in _bbs :
		for inst in bb :
			result[inst] = (set(), set())

	for bb in _bbs :
		for inst in bb :
			if inst.reads() :
				for var in inst.reads() :
					if var.startswith("$") :
						for anti_source in get_first_write(inst, bb, var):
							result[inst][0].add(anti_source)
							result[anti_source][1].add(inst)
					else :
						print("#TODO: memory dependency")  # TODO memroy
	return result

def build_output_dependency_graph(asm):
	'''Builds the graph of output dependency relation. We say that statement B
	output depends on statement A, when A writes the value that is also written
	by B.
	@param asm : AsmInfo instance, holding the data about the given assembly code.
	@return : A map from instructions to the (next, prev) tuple, where next and
	prev are the set of descendants and ancestors respectively.
	'''

	result = {}

	for bb in _bbs :
		for inst in bb :
			result[inst] = (set(), set())

	for bb in _bbs :
		for inst in bb :
			if inst.writes() :
				for var in inst.writes() :
					if var.startswith("$") :
						for output_source in get_first_write(inst, bb, var):
							result[inst][0].add(output_source)
							result[output_source][1].add(inst)
					else :
						print("#TODO: memory dependency")  # TODO memroy
	return result

def build_dependency_graph(asm):
	'''Build the DDG (Directed Dependency Graph) for the given assembly code.
	@param asm : AsmInfo instance holding the data about the assembly code.
	@return : A map from instructions to the (next, prev) pair, where next and
	prev are the set of descendants and ancestors, respectively.
	'''

	global _proc_headers
	global _bbs
	global _cache_last_write
	global _cache_first_write
	global _UNKNOWN
	_proc_headers = {asm.instructions.labels[lbl] for lbl in get_procedures(asm)}
	_bbs = get_basic_blocks(asm)
	_cache_last_write = {}
	_cache_first_write = {}
	_UNKNOWN = {}

	true_dependency = build_true_dependency_graph(asm)
	anti_dependency = build_anti_dependency_graph(asm)
	output_dependency = build_output_dependency_graph(asm)
	result = {inst:(true_dependency[inst][0].union(anti_dependency[inst][0]).union(output_dependency[inst][0]),
			true_dependency[inst][1].union(anti_dependency[inst][1]).union(output_dependency[inst][1]))
			for inst in asm.instructions}

	return result
