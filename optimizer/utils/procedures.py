'''
Created on Jan 25, 2013

@copyright: 
Copyright (C) Us5ive!
This code is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3, or (at your option)
any later version.

This code is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this code; see the file COPYING.  If not, write to
the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
'''

from optimizer.utils.BasicBlock import get_basic_blocks
import collections

def get_procedures(asm):
	'''Locates procedures in the given assembly code.
	@param asm: AsmInfo instance holding the data about the assembly code.
	@return : a set of labels, each of which defines a procedure.
	'''

	temp = set()
	headers = set()
	label2bb = {}
	bbs = get_basic_blocks(asm)

	for bb in bbs :
		if bb.first.label != None :
			label2bb[bb.first.label] = bb

	headers.add(label2bb["main"])
	mark = { bb:False for bb in bbs }
	while headers.__len__() != 0 :
		header = headers.pop()
		if header in temp :
			continue
		temp.add(header)
		q = collections.deque()
		q.appendleft(header)

		while q.__len__() :
			bb = q.pop()
			if not mark[bb] :
				mark[bb] = True
				for next in bb.next :
					q.appendleft(next)
				for inst in bb :
					if inst.name in {"bgezal", "bltzal", "jal"} :
						headers.add(label2bb[inst.get_target_address()])
					elif inst.name == "jalr" :
						print("oops, jalr is not supported.")

	return {bb.first.label for bb in temp}

def build_interprocedural_dominance_relation(asm, bbs=None):
	'''Maps each basic block in the Assembly file, to the set of its dominators.
	@param asm : AsmInfo instance, holding the data about the assembly file.
	@param bbs : Basic blocks of the code. (All of them!). To be used as the key
	to the output map. If not supplied, it will be calculated. Thus you can
	ignore this argument.
	@return : A map that maps each basic block to the set of its dominators.
	'''

	label2bb = {}
	result = {}

	if bbs == None :
		bbs = optimizer.utils.BasicBlock.get_basic_blocks(asm)

	# Get basic blocks
	for bb in bbs :
		result[bb] = set()
		if bb.first.label :
			label2bb[bb.first.label] = bb

	# Build relation
	for bb in bbs :
		for temp in bbs :
			result[bb].add(temp)
	for proc_name in get_procedures(asm) :
		proc_head = label2bb[proc_name]
		result[proc_head] = {proc_head}
	for i in range(0, bbs.__len__() * bbs.__len__()) :
		for bb in bbs :
			for prev in bb.prev :
				result[bb] = result[bb].intersection(result[prev])
			result[bb].add(bb)

	return result

def build_interprocedural_post_domination_relation(asm, bbs=None):
	'''Maps each basic block in the Assembly file, to the set of its 
	post-dominators.
	@param asm : AsmInfo instance, holding the data about the assembly file.
	@param bbs : Basic blocks of the code. (All of them!). To be used as the key
	to the output map. If not supplied, it will be calculated. Thus you can
	ignore this argument.
	@return : A map that maps each basic block to the set of its post-dominators.
	'''

	label2bb = {}
	result = {}

	if bbs == None :
		bbs = optimizer.utils.BasicBlock.get_basic_blocks(asm)

	# Get basic blocks
	for bb in bbs :
		result[bb] = set()
		if bb.first.label :
			label2bb[bb.first.label] = bb

	# Build relation
	for bb in bbs :
		if len(bb.next) != 0 :
			for temp in bbs :
				result[bb].add(temp)
		else :
			result[bb].add(bb)

	for i in range(0, bbs.__len__() * bbs.__len__()) :
		for bb in bbs :
			for next in bb.next :
				result[bb] = result[bb].intersection(result[next])
			result[bb].add(bb)

	return result
