'''
Created on Jan 17, 2013

@copyright: 
Copyright (C) Us5ive!
This code is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation; either version 3, or (at your option) any later version.

This code is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this code; see the file COPYING.  If not, write to the Free Software Foundation,
675 Mass Ave, Cambridge, MA 02139, USA.
'''

import optimizer.loop_unrolling

class BasicBlock(object) :
	first = None  # First instruction of the block
	last = None  # Last instruction of the block
	label = None  # Label that begins the block (or None)
	next = None  # Edges exiting this node
	prev = None  # Edges entering this node
	num = -1
	
	def __init__(self, first, last):
		self.next = []
		self.prev = []
		self.first = first
		self.last = last

	def __str__(self):
		return "<%s, %s>" % (self.first, self.last)

	def __len__(self):
		return ((self.last.address - self.first.address) >> 2) + 1

	def __getitem__(self, key):
		if key >= self.first.address :
			key = (key - self.first.address) >> 2
		if key >= len(self) :
			raise StopIteration

		result = self.first
		for i in range(0, key) :
			if result :
				result = result.next
		return result
	
def get_basic_blocks(asm):
	'''Divides the program into basic blocks.
	@return: List of basic blocks in form of (start, end).
	'''

	node = asm.instructions.root
	result = []
	while node :
		start = node
		while not node.is_brancher_wo_return()  and \
		not node.is_unconditional_jumper_wo_return() and \
		node.next and not node.next.label:
			node = node.next
		end = node
		result.append(BasicBlock(start, end))
		node = node.next

	for i in range(0, len(result) - 1) :
		if not result[i].last.is_unconditional_jumper_wo_return() :
			result[i].next.append(result[i + 1])
			result[i + 1].prev.append(result[i])

	for block in result :
		if block.last.is_unconditional_jumper_wo_return() or \
		block.last.is_brancher_wo_return() :
			target = block.last.get_target_address()
			if target and not target.startswith("$") :  # TODO or if the value in target register is constant...
				for bb in result :
					if bb.first.label == target :
						block.next.append(bb)
						bb.prev.append(block)
						break
	i=0
	for block in result :
		if block.last.name == "syscall" and \
		optimizer.loop_unrolling._get_value("$2", block, len(block) - 1) == 10 :
			for next_ in block.next :
				next_.prev.remove(block)
			block.next = []
		block.num = i
		i+=1
	
	return result
